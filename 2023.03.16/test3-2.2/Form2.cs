﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyGIS;

namespace test3_2._2
{

#pragma warning disable CS1591 // 缺少对公共可见类型或成员的 XML 注释
    public partial class Form2 : Form
    {
        GISView view = null;
        List<GISFeature> features = new List<GISFeature>();
        public Form2()
        {
            InitializeComponent();
            view = new GISView(new GISExtent(new GISVertex(0, 0), new GISVertex(100, 100)), ClientRectangle);

        }

        /// <summary>
        /// “添加点实体”点击事件
        /// </summary>
        private void AddPoint_Click(object sender, EventArgs e)
        {
            double x = Convert.ToDouble(XInput.Text);
            double y = Convert.ToDouble(YInput.Text);
            GISVertex onevertex = new GISVertex(x, y);
            GISPoint onepoint = new GISPoint(onevertex);
            string attribute = AttInput.Text;
            GISAttribute oneattribute = new GISAttribute();
            oneattribute.AddValue(attribute);
            GISFeature onefeature = new GISFeature(onepoint, oneattribute);
            features.Add(onefeature);
            Graphics graphics = this.CreateGraphics();
            onefeature.draw(graphics, view, true, 0);
        }


        /// <summary>
        /// 在绘图窗体中通过点击鼠标查询属性值
        /// </summary>
        private void Form2_MouseClick(object sender, MouseEventArgs e)
        {
            GISVertex mouselocation = view.ToMapVertex(new Point(e.X, e.Y));//将鼠标的点击位置转换为地图坐标
            double mindistance = double.MaxValue;
            int findid = -1;
            for (int i = 0; i < features.Count; i++)
            {
                double onedistance = features[i].spatialpart.centroid.Distance(mouselocation);//计算鼠标点击位置到空间要素中心点的距离
                if (onedistance < mindistance)
                {
                    mindistance = onedistance;
                    findid = i;
                }
            }
            if (findid == -1)
            {
                MessageBox.Show("没有任何空间对象！");
                return;
            }
            //此时完成mindistance的计算，因为这里的单位是地图坐标，不能直接与像素进行比较，故调用ToScreenPoint函数
            Point nearestpoint = view.ToScreenPoint(features[findid].spatialpart.centroid);//将最近的空间中心点转换为屏幕坐标
            int screendistance = Math.Abs(nearestpoint.X - e.X) + Math.Abs(nearestpoint.Y - e.Y);//Math.Abs表示取得绝对值
            if (screendistance > 5)
            {
                MessageBox.Show("请靠近空间对象点击！");
                return;
            }
            MessageBox.Show("该属性的空间对象是" + features[findid].getAttribute(0));
        }


        /// <summary>
        /// “更新地图”点击事件
        /// </summary>
        private void UpdateMap_MouseClick(object sender, MouseEventArgs e)
        {
            double minx = Double.Parse(MinXInput.Text);
            double miny = Double.Parse(MinYInput.Text);
            double maxx = Double.Parse(MaxXInput.Text);
            double maxy = Double.Parse(MaxYInput.Text);
            view.Update(new GISExtent(new GISVertex(minx, miny), new GISVertex(maxx, maxy)), ClientRectangle);
            Graphics graphics = CreateGraphics();
            graphics.FillRectangle(new SolidBrush(Color.Black), ClientRectangle);//使用指定颜色填充地图框内部
            for (int i = 0; i < features.Count; i++)
            {
                features[i].draw(graphics, view, true, 0);//在新的屏幕坐标系下重绘各点实体
            }
        }



        /// <summary>
        /// “大量点实体”点击事件
        /// </summary>
        private void MassivePoints_Click(object sender, EventArgs e)//生成大量点实体
        {
            int a = 1000;
            for (int i = 0; i < a; i++)
            {

                byte[] buffer = Guid.NewGuid().ToByteArray();
                int iSeed = BitConverter.ToInt32(buffer, 0);
                Random random1 = new Random(iSeed);
                int x1 = random1.Next(-180, 180);
                byte[] buffer2 = Guid.NewGuid().ToByteArray();
                int iSeed2 = BitConverter.ToInt32(buffer2, 0);
                Random random2 = new Random(iSeed2);
                int y1 = random1.Next(-85,85);
                string attribute = "";
                GISAttribute oneattribute = new GISAttribute();
                oneattribute.AddValue(attribute);
                GISVertex onevertex = new GISVertex(x1, y1);
                GISPoint onepoint = new GISPoint(onevertex);
                GISFeature onefeature = new GISFeature(onepoint, oneattribute); //（已解决）程序认为随机生成的点不存在（无法点击查询属性），因为未添加到GISFeature类
                features.Add(onefeature);
                Graphics graphics = this.CreateGraphics();
                onefeature.draw(graphics, view,true,0);
            }
        }
    }
}
