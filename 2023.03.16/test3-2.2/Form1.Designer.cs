﻿
namespace test_2_2
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.CreateVertex = new System.Windows.Forms.Button();
            this.AttInput = new System.Windows.Forms.TextBox();
            this.XInput = new System.Windows.Forms.TextBox();
            this.YInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CreateVertex
            // 
            this.CreateVertex.Location = new System.Drawing.Point(1108, 20);
            this.CreateVertex.Name = "CreateVertex";
            this.CreateVertex.Size = new System.Drawing.Size(136, 63);
            this.CreateVertex.TabIndex = 0;
            this.CreateVertex.Text = "生成点实体";
            this.CreateVertex.UseVisualStyleBackColor = true;
            this.CreateVertex.Click += new System.EventHandler(this.CreateVertex_Click);
            // 
            // AttInput
            // 
            this.AttInput.Location = new System.Drawing.Point(804, 32);
            this.AttInput.Name = "AttInput";
            this.AttInput.Size = new System.Drawing.Size(100, 31);
            this.AttInput.TabIndex = 1;
            // 
            // XInput
            // 
            this.XInput.Location = new System.Drawing.Point(172, 32);
            this.XInput.Name = "XInput";
            this.XInput.Size = new System.Drawing.Size(100, 31);
            this.XInput.TabIndex = 2;
            // 
            // YInput
            // 
            this.YInput.Location = new System.Drawing.Point(478, 31);
            this.YInput.Name = "YInput";
            this.YInput.Size = new System.Drawing.Size(100, 31);
            this.YInput.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(131, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(437, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(722, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 21);
            this.label3.TabIndex = 6;
            this.label3.Text = "属性";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1314, 784);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.YInput);
            this.Controls.Add(this.XInput);
            this.Controls.Add(this.AttInput);
            this.Controls.Add(this.CreateVertex);
            this.Name = "Form1";
            this.Text = "Form1";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreateVertex;
        private System.Windows.Forms.TextBox AttInput;
        private System.Windows.Forms.TextBox XInput;
        private System.Windows.Forms.TextBox YInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

