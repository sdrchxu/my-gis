﻿
namespace test3_2._2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.XInput = new System.Windows.Forms.TextBox();
            this.MaxYInput = new System.Windows.Forms.TextBox();
            this.MaxXInput = new System.Windows.Forms.TextBox();
            this.MinYInput = new System.Windows.Forms.TextBox();
            this.MinXInput = new System.Windows.Forms.TextBox();
            this.AttInput = new System.Windows.Forms.TextBox();
            this.YInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.AddPoint = new System.Windows.Forms.Button();
            this.UpdateMap = new System.Windows.Forms.Button();
            this.MassivePoints = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // XInput
            // 
            this.XInput.Location = new System.Drawing.Point(195, 28);
            this.XInput.Name = "XInput";
            this.XInput.Size = new System.Drawing.Size(100, 31);
            this.XInput.TabIndex = 0;
            // 
            // MaxYInput
            // 
            this.MaxYInput.Location = new System.Drawing.Point(886, 116);
            this.MaxYInput.Name = "MaxYInput";
            this.MaxYInput.Size = new System.Drawing.Size(100, 31);
            this.MaxYInput.TabIndex = 1;
            // 
            // MaxXInput
            // 
            this.MaxXInput.Location = new System.Drawing.Point(625, 116);
            this.MaxXInput.Name = "MaxXInput";
            this.MaxXInput.Size = new System.Drawing.Size(100, 31);
            this.MaxXInput.TabIndex = 2;
            // 
            // MinYInput
            // 
            this.MinYInput.Location = new System.Drawing.Point(379, 116);
            this.MinYInput.Name = "MinYInput";
            this.MinYInput.Size = new System.Drawing.Size(100, 31);
            this.MinYInput.TabIndex = 3;
            // 
            // MinXInput
            // 
            this.MinXInput.Location = new System.Drawing.Point(133, 116);
            this.MinXInput.Name = "MinXInput";
            this.MinXInput.Size = new System.Drawing.Size(100, 31);
            this.MinXInput.TabIndex = 4;
            // 
            // AttInput
            // 
            this.AttInput.Location = new System.Drawing.Point(754, 28);
            this.AttInput.Name = "AttInput";
            this.AttInput.Size = new System.Drawing.Size(100, 31);
            this.AttInput.TabIndex = 5;
            // 
            // YInput
            // 
            this.YInput.Location = new System.Drawing.Point(451, 28);
            this.YInput.Name = "YInput";
            this.YInput.Size = new System.Drawing.Size(100, 31);
            this.YInput.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(168, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 21);
            this.label1.TabIndex = 7;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(319, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 21);
            this.label2.TabIndex = 8;
            this.label2.Text = "MinY";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 21);
            this.label3.TabIndex = 9;
            this.label3.Text = "MinX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(696, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 21);
            this.label4.TabIndex = 10;
            this.label4.Text = "属性";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(424, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 21);
            this.label5.TabIndex = 11;
            this.label5.Text = "Y";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(826, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 21);
            this.label6.TabIndex = 12;
            this.label6.Text = "MaxY";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(565, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 21);
            this.label7.TabIndex = 13;
            this.label7.Text = "MaxX";
            // 
            // AddPoint
            // 
            this.AddPoint.Location = new System.Drawing.Point(906, 23);
            this.AddPoint.Name = "AddPoint";
            this.AddPoint.Size = new System.Drawing.Size(163, 37);
            this.AddPoint.TabIndex = 14;
            this.AddPoint.Text = "添加点实体";
            this.AddPoint.UseVisualStyleBackColor = true;
            this.AddPoint.Click += new System.EventHandler(this.AddPoint_Click);
            // 
            // UpdateMap
            // 
            this.UpdateMap.Location = new System.Drawing.Point(1013, 110);
            this.UpdateMap.Name = "UpdateMap";
            this.UpdateMap.Size = new System.Drawing.Size(143, 37);
            this.UpdateMap.TabIndex = 15;
            this.UpdateMap.Text = "更新地图";
            this.UpdateMap.UseVisualStyleBackColor = true;
            this.UpdateMap.MouseClick += new System.Windows.Forms.MouseEventHandler(this.UpdateMap_MouseClick);
            // 
            // MassivePoints
            // 
            this.MassivePoints.Location = new System.Drawing.Point(1013, 169);
            this.MassivePoints.Name = "MassivePoints";
            this.MassivePoints.Size = new System.Drawing.Size(143, 42);
            this.MassivePoints.TabIndex = 16;
            this.MassivePoints.Text = "大量点实体";
            this.MassivePoints.UseVisualStyleBackColor = true;
            this.MassivePoints.Click += new System.EventHandler(this.MassivePoints_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1183, 724);
            this.Controls.Add(this.MassivePoints);
            this.Controls.Add(this.UpdateMap);
            this.Controls.Add(this.AddPoint);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.YInput);
            this.Controls.Add(this.AttInput);
            this.Controls.Add(this.MinXInput);
            this.Controls.Add(this.MinYInput);
            this.Controls.Add(this.MaxXInput);
            this.Controls.Add(this.MaxYInput);
            this.Controls.Add(this.XInput);
            this.Name = "Form2";
            this.Text = "Form2";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form2_MouseClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox XInput;
        private System.Windows.Forms.TextBox MaxYInput;
        private System.Windows.Forms.TextBox MaxXInput;
        private System.Windows.Forms.TextBox MinYInput;
        private System.Windows.Forms.TextBox MinXInput;
        private System.Windows.Forms.TextBox AttInput;
        private System.Windows.Forms.TextBox YInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button AddPoint;
        private System.Windows.Forms.Button UpdateMap;
        private System.Windows.Forms.Button MassivePoints;
    }
}