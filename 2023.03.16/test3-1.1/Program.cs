﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test3_1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Vehicle v1 = new Car(80, 5);//Car（速度，乘客数）
            v1.Run(800);//Run（距离）
            Vehicle v2 = new Truck(80);//Truck（速度）
            v2.Run(800);
        }

        public abstract class Vehicle//创建Vehicle类
        {
            public float V//速度
            {
                get;
                set;
            }

            public Vehicle(float v)
            {
                V = v;
            }


            public virtual void Run(float d)
            {
                Console.WriteLine("行驶{0}公里用时{1}", d, d / V);
            }
        }

        public class Car : Vehicle//创建Vehicle的子类，Car
        {
            public Car(float V, int Passengers) : base(V)
            {
            }
        }

        public class Truck : Vehicle//创建Vehicle的子类，Truck
        {
            float load=200;
            public Truck(float V) : base(V)
            {

            }
            public override void Run(float d)
            {
                Console.WriteLine("行驶{0}公里用时{1}",d,(1+load/100)*d/V);
            }
        }
    }
}
