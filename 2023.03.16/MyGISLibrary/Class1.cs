﻿using MyGIS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace MyGIS
{

#pragma warning disable CS1591 // 缺少对公共可见类型或成员的 XML 注释
    public class GISVertex   //节点类
    {
        public double x;
        public double y;
        public GISVertex(double _x, double _y)
        {
            x = _x;
            y = _y;
        }//外部传参，向x与y赋值
        public double Distance(GISVertex anothervertex)
        {
            return Math.Sqrt((x - anothervertex.x) *
                (x - anothervertex.x) + (y - anothervertex.y) * (y - anothervertex.y));
        }//计算两点间距离
    }

    /// <summary>
    /// 点实体类
    /// </summary>
    public class GISPoint : GISSpatial  //点实体
    {

        public GISPoint(GISVertex onevertex)
        {
            centroid = onevertex;
            extent = new GISExtent(onevertex, onevertex);
        }//实例化点实体
        public override void draw(Graphics graphics,GISView view)
        {
            Point screenpoint = view.ToScreenPoint(centroid);
            graphics.FillEllipse(new SolidBrush(Color.Red),
                new Rectangle(screenpoint.X - 3, screenpoint.Y - 3, 6, 6));
        }//在屏幕上画出点实体
        public double Distance(GISVertex anothervertex)
        {
            return centroid.Distance(anothervertex);
        }//计算两点间距离
    }
    public class GISLine : GISSpatial
    {
        List<GISVertex> AllVertexs;
        public override void draw(Graphics graphics,GISView view)
        {

        }
    }
    public class GISPolygon : GISSpatial
    {
        List<GISVertex> AllVertexs;
        public override void draw(Graphics graphics,GISView view)
        {

        }
    }

    /// <summary>
    /// 空间对象特征类
    /// </summary>
    public class GISFeature   //空间要素特征类
    {
        public GISSpatial spatialpart;  //对象的空间信息
        public GISAttribute attributepart;  //对象的属性信息
        public GISFeature(GISSpatial spatial, GISAttribute attribute)
        {
            spatialpart = spatial;
            attributepart = attribute;
        }
        public void draw(Graphics graphics,GISView view, bool DrawAttributeOrNot, int index)
        {
            spatialpart.draw(graphics,view);
            if (DrawAttributeOrNot)
            {
                attributepart.draw(graphics,view, spatialpart.centroid, index);
            }
        }
        public object getAttribute(int index)
        {
            return attributepart.GetValue(index);
        }
    }

    /// <summary>
    /// 空间对象属性类
    /// </summary>
    public class GISAttribute
    {
        public ArrayList values = new ArrayList();
        public void AddValue(object o)  //添加属性值
        {
            values.Add(o);
        }
        public object GetValue(int index)  //获取属性值
        {
            return values[index];
        }
        public void draw(Graphics graphics, GISView view, GISVertex location,int index)
        {
            Point screenpoint = view.ToScreenPoint(location);
            graphics.DrawString(values[index].ToString(),
                new Font("宋体", 20),
                new SolidBrush(Color.Green), 
                new PointF(screenpoint.X,screenpoint.Y));
        }//画出属性信息
    }


    public abstract class GISSpatial  //抽象空间要素类
    {
        public GISVertex centroid;//空间实体的中心点
        public GISExtent extent;//空间范围：最小外接矩形
        public abstract void draw(Graphics graphics,GISView view);
    }

    /// <summary>
    /// 空间对象范围类
    /// </summary>
    public class GISExtent  
    {
        public GISVertex bottomleft;
        public GISVertex upright;
        public GISExtent(GISVertex _bottomleft, GISVertex _upright)
        {
            bottomleft = _bottomleft;
            upright = _upright;
        }
        public double getMinX()
        {
            return bottomleft.x;
        }
        public double getMaxX()
        {
            return upright.x;
        }
        public double getMinY()
        {
            return bottomleft.y;
        }
        public double getMaxY()
        {
            return upright.y;
        }
        public double getWidth()
        {
            return upright.x - bottomleft.x;
        }
        public double getHeight()
        {
            return upright.y - bottomleft.y;
        }
    }


    /// <summary>
    /// 空间对象浏览类
    /// </summary>
    public class GISView   //空间对象浏览类（实现屏幕坐标与地图坐标的相互转换）
    {
        GISExtent CurrentMapExtent;  //记录显示的地图范围
        Rectangle MapWindowSize;  //记录绘图窗口的大小 
        double MapMinX, MapMinY;
        int WinW, WinH;
        double MapW, MapH;
        double ScaleX, ScaleY;
        public GISView(GISExtent _extent, Rectangle _rectangle)//记录当前显示范围
        {
            Update(_extent, _rectangle);
        }
        public void Update(GISExtent _extent, Rectangle _rectangle)
        {
            CurrentMapExtent = _extent;//当前地图范围
            MapWindowSize = _rectangle;
            MapMinX = CurrentMapExtent.getMinX();//地图最小X值
            MapMinY = CurrentMapExtent.getMinY();
            WinW = MapWindowSize.Width;//窗口宽度
            WinH = MapWindowSize.Height;//窗口高度
            MapW = CurrentMapExtent.getWidth();
            MapH = CurrentMapExtent.getHeight();
            ScaleX = MapW / WinW;//比例尺=图上距离/实际距离
            ScaleY = MapH / WinH;
        }//更新地图

        /// <summary>
        /// 转换为屏幕坐标系
        /// </summary>
        /// <param name="onevertex"></param>
        /// <returns></returns>
        public Point ToScreenPoint(GISVertex onevertex)//转换为屏幕坐标系
        {
            double ScreenX = (onevertex.x - MapMinX) / ScaleX;
            double ScreenY = WinH-(onevertex.y - MapMinY) / ScaleY;
            return new Point((int)ScreenX, (int)ScreenY);
        }

        public GISVertex ToMapVertex(Point point)//转换为地图坐标系
        {
            double MapX = ScaleX*point.X + MapMinX;
            double MapY =ScaleY * (WinH-point.Y) + MapMinY;
            return new GISVertex(MapX, MapY);
        }
    }

}
