﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test3_1._2
{
    public partial class StudentForm : Form
    {
        private Student student;
        private Graduate graduate;
        private UnderGraduate undergraduate;
        public StudentForm(Student student)
        {
            this.student = student;
            InitializeComponent();
        }

        private void StudentForm_Load(object sender, EventArgs e)
        {
            IDBox.Text = student.ID;
            NameBox.Text = student.Name;
            if (student.Gender == true)
                SexBox.Text = "男";
            else
                SexBox.Text = "女";
            GradeBox.Text = student.Grade.ToString();
            ClassBox.Text = student.Class.ToString();
            if (student is Graduate graduate)
            {
                DepartmentBox.Text = graduate.Department;
                TutorBox.Text = graduate.Tutor;
            }
            else if (student is UnderGraduate undergraduate)
                DepartmentBox.Text = undergraduate.Department;

        }//重大问题（已解决）：当学生为Graduate/Undergraduate类型时，无法显示其导师或专业
    }
}
