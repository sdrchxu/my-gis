﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test3_1._2
{
    public class Student
    {
        private string id, name;
        private int _class, grade;
        private bool gender;

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Class
        {
            get { return _class; }
            set { _class = value; }
        }

        public virtual int Grade  //虚拟属性，便于在子类中进行重写
        {
            get { return grade; }
            set { grade = value; }
        }

        public bool Gender
        {
            get { return gender; }
            set { gender = value; }
        }


        public Student(string id, string name, bool gender, int _class = 1, int grade = 1)
        {
            this.id = id;
            this.name = name;
            this.gender = gender;
            this._class = _class;
            this.grade = grade;
        }


        //public override string Tostring()  //CS0015:没有找到合适的方法来重写
        //{                                  //ToString而非Tostring
        //    return id + name;
        //}

        public override string ToString()
        {
            return id + name;
        }
    }

    public class UnderGraduate : Student
    {
        public string Department { get; set; }
        public UnderGraduate(string id, string name, bool gender, int _class = 1, int Grade=1,string Department = "") : base(id, name, gender, _class)
        {
            this.Department = Department;//已解决bug：输入department未正确赋值
            this.Grade = Grade;
        }
        public override int Grade
        {
            get => base.Grade;
            set
            {
                if (value > 4)
                    base.Grade = 4;
                else if (value < 1)
                    base.Grade = 1;
                else base.Grade = value;

            }
        }

    }

    public class Graduate : Student
    {
        public string Department { get; set; }
        public string Tutor { get; set; }
        public Graduate(string id, string name, bool gender, int _class = 1, int Grade = 1,string department="",string tutor="") : base(id, name, gender, _class)
        {
            this.Department = department;
            this.Tutor = tutor;   //已解决bug：输入department和tutor后无法正确赋值，原因：没有写this.（未传递给基类的构造函数进行初始化）
            this.Grade = Grade;  //已解决bug：输入grade后无法正确赋值
        }
        public override int Grade
        {
            get => base.Grade;
            set
            {
                if (value > 3)
                    base.Grade = 3;
                else if (value < 1)
                    base.Grade = 1;
                else base.Grade = value;
            }
        }
    }
}
