﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test3_1._2
{
    public partial class Form1 : Form
    {
        private Student[] students;
        public Form1()
        {
            InitializeComponent();
            students = new Student[5];
            students[0] = new Student("1001", "赵华", false);
            students[1] = new Student("1002", "彭小飞", true, 3);
            students[2] = new Student("1003", "王萌", false, 1, 4);
            students[3] = new UnderGraduate("1004", "王小明", true, 5, 10,"文学院");
            students[4] = new Graduate("1005", "张珊", false, 3, 2, "地球科学学院", "赵飞");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
                this.listBox1.Items.Add(students[i]);
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            StudentForm form = new StudentForm(students[listBox1.SelectedIndex]);
            form.ShowDialog();
        }
    }

}
