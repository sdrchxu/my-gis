using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace WindowsApplication1
{
	/// <summary>
	/// Form1 的摘要说明。
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Button button4;
		private System.ComponentModel.IContainer components;

		public Form1()
		{
			//
			// Windows 窗体设计器支持所必需的
			//
			InitializeComponent();

			//
			// TODO: 在 InitializeComponent 调用后添加任何构造函数代码
			//
		}

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows 窗体设计器生成的代码
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.button4 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(134)));
			this.button1.Location = new System.Drawing.Point(88, 112);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(104, 32);
			this.button1.TabIndex = 0;
			this.button1.Text = "说你好";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(80, 152);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(128, 40);
			this.button2.TabIndex = 1;
			this.button2.Text = "测试";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(40, 48);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(120, 21);
			this.textBox1.TabIndex = 2;
			this.textBox1.Text = "textBox1";
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(40, 72);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(120, 21);
			this.textBox2.TabIndex = 3;
			this.textBox2.Text = "textBox2";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(184, 72);
			this.button3.Name = "button3";
			this.button3.TabIndex = 4;
			this.button3.Text = "button3";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Red;
			this.label1.Location = new System.Drawing.Point(16, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 24);
			this.label1.TabIndex = 5;
			this.label1.Text = "label1";
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(48, 224);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(88, 21);
			this.textBox3.TabIndex = 6;
			this.textBox3.Text = "textBox3";
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(200, 224);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(56, 24);
			this.button4.TabIndex = 7;
			this.button4.Text = "button4";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(292, 270);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show( "你好"); //这是使用弹出信息框
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			button2.Text = "aaaa"; //字符串
			button2.Text = DateTime.Now.ToString(); //日期时间及ToString
			
			this.BackColor = Color.FromArgb(255, 255, 0 ); //颜色
			this.button2.Width = 100; //宽度
		}

		//鼠标移动事件
		private void Form1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			this.Text = e.X + "," + e.Y; //字符串连接		
		}

		//文本改变事件
		private void textBox1_TextChanged(object sender, System.EventArgs e)
		{
			textBox2.Text = textBox1.Text; //两个文本框显示相同字符
		}


		/// <summary>
		/// 这里是使用xml的注释
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button3_Click(object sender, System.EventArgs e)
		{
		}

		Random rnd = new Random(); //这是使用随机类

		//这是使用Timer对象，能“自动”周期性发出事件。 注意其Enabled属性要为True才能起作用
		private void timer1_Tick(object sender, System.EventArgs e) 
		{
			//this.label1.Left = rnd.Next(300);
			this.label1.Left += 4;
			this.BackColor = Color.FromArgb( rnd.Next(255), rnd.Next(255), rnd.Next(255)); //这是使用随机数
		
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			string s = this.textBox3.Text.Trim(); //文本框用于输入

			double a = Convert.ToDouble(s);  //这是使用类型的转换
			//a = double.Parse( s );

			double root = Math.Sqrt( a ); //数学函数都在Math类中

			this.label1.Text = 
				string.Format( "数{0}的平方根是{1:#.###}", a, root );//这是使用格式串 //标签用于输出

			//WL( DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") ); //这是使用格式串
			//WL( string.Format("{0:##.#}", 3.14 ) );

		}
	}
}
