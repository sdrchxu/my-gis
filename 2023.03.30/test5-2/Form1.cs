﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyGIS;

namespace test5_2
{
    public partial class Form1 : Form
    {
        GISLayer Layer = null;
        GISView View = null;
        public Form1()
        {
            InitializeComponent();
            View = new GISView(new GISExtent(new GISVertex(0, 0), new GISVertex(100, 100)), ClientRectangle);
        }

        private void OpenFileButton_Click(object sender, EventArgs e)
        {
            GISShapefile shapefile = new GISShapefile();
            Layer = shapefile.ReadShapefile(@"D:\Lenovo\Desktop\云南大学\GIS空间分析原理与方法\实验一\exp7\data_exp7\drinking.shp");/*此处填写@+"文件路径"*/
            Layer.DrawAttributeOrNot = false;
            MessageBox.Show("read " + Layer.FeatureCount() + " point objects");//提示读取到的点实体的数量
        }

        private void ShowMapButton_Click(object sender, EventArgs e)
        {
            View.UpdateExtent(Layer.Extent);
            UpdateMap();
        }

        private void UpdateMap()
        {
            Graphics graphics = CreateGraphics();
            graphics.FillRectangle(new SolidBrush(Color.Black), ClientRectangle);
            Layer.draw(graphics,View);
        }

        private void MapButtonClick(object sender,EventArgs e)
        {
            GISMapActions actions = GISMapActions.zoomin;
            if ((Button)sender == ZoomInButton) actions = GISMapActions.zoomin;
            else if ((Button)sender == ZoomOutButton) actions = GISMapActions.zoomout;
            else if ((Button)sender == MoveDownButton) actions = GISMapActions.movedown;
            else if ((Button)sender == MoveUpButton) actions = GISMapActions.moveup;
            else if ((Button)sender == MoveLeftButton) actions = GISMapActions.moveleft;
            else if ((Button)sender == MoveRightButton) actions = GISMapActions.moveright;
            View.ChangeView(actions);
            UpdateMap();
        }
    }
}
