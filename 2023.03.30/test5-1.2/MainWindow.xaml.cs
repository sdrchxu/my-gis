﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace test5_1._2
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer1;
        public MainWindow()
        {
            InitializeComponent();
            timer1 = new DispatcherTimer();
            timer1.Interval = TimeSpan.FromMilliseconds(10);//触发的时间间隔
            timer1.Tick += new EventHandler(timer1_Tick);//触发事件
            timer1.IsEnabled = true;//使计时器运行
        }

        void timer1_Tick(object sender,EventArgs e)
        {
            if (ProgressBar1.Value == 100)
                ProgressBar1.Value = 0;
            else
                ProgressBar1.Value += 1;
            this.Title = string.Format("当前进度{0}%", ProgressBar1.Value);//将窗口标题设置为当前进度条的值
        }
    }

}
