﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace test5_1._4
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)//保证文本框中只有数字和最多一个小数点
        {
            if ((e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal)
            {
                if (TextBox1.Text.Contains(".") && e.Key == Key.Decimal)
                    e.Handled = true;
                else
                    e.Handled = false;
            }
            else if(((e.Key>=Key.D0&&e.Key<=Key.D9)||e.Key==Key.OemPeriod)&&e.
                KeyboardDevice.Modifiers != ModifierKeys.Shift)
            {
                if (TextBox1.Text.Contains(".") && e.Key == Key.OemPeriod)
                    e.Handled = true;
                else
                    e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
