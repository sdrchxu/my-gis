﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multicast
{
    public partial class Form1 : Form
    {



        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)//点击画图按钮的事件
        {
            Graphics g = this.CreateGraphics();
            Fun fun = null;
            Pen pen = Pens.Blue;
            fun += new Fun(this.Squre);//多播
            fun += new Fun(this.XPlus);
            fun += new Fun(Math.Cos);
            fun += new Fun(Math.Sqrt);
            PlotFun(fun, g, pen);
        }
        delegate double Fun(double x);

        void PlotFun(Fun fun, Graphics graphics, Pen pen)//画出函数曲线
        {
            Pen pens = Pens.Blue;
            Graphics g = this.CreateGraphics();
            foreach (Fun fun1 in fun.GetInvocationList())
                for (double x = 0; x < 30; x += 0.1)
                {
                    double y = fun1(x);
                    Point point = new Point((int)(x * 20), (int)(200 - y * 30));
                    g.DrawLine(pens, point, new Point(point.X + 2, point.Y + 2));
                    Console.WriteLine("" + x + "" + y);
                }
        }
        double Squre(double x)
        {
            return (Math.Sqrt(Math.Sqrt(x)));
        }
        double XPlus(double x)
        {
            return (Math.Sin(x) + Math.Cos(x * 5) / 5);
        }
    }
}

