﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyGIS;
using static MyGIS.GISMyFile;

namespace Midterm_test
{
    public partial class Form1 : Form
    {
        GISLayer Layer = null;
        GISView View = null;
        ShowAttribute AttributeWindow = null;
        Bitmap BackWindow;
        string ShapefilePath=null;
        string TxtFilePath = null;
        
        public Form1()
        {
            InitializeComponent();
            View = new GISView(new GISExtent(new GISVertex(0, 0), new GISVertex(100, 100)), ClientRectangle);
        }

        private void OpenFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Shapefile 文件|*.shp";//文件类型限制为Shapefile
            openFileDialog.RestoreDirectory = false;//不还原选择文件时更改的目录
            openFileDialog.FilterIndex = 1;
            openFileDialog.Multiselect = false;//不允许同时选中多个文件
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;
            ShapefilePath = openFileDialog.FileName;
            Layer = GISShapefile.ReadShapefile(openFileDialog.FileName);
            Layer.DrawAttributeOrNot = false;
            MessageBox.Show("read" + Layer.FeatureCount() + "objects.");
            View.UpdateExtent(Layer.Extent);
            UpdateMap();
        }


        private void ShowMapButton_Click(object sender, EventArgs e)
        {
            if (Layer == null)
            {
                MessageBox.Show("目前没有加载图层，无法更新");
            }
            else
            {
                View.UpdateExtent(Layer.Extent);
                UpdateMap();
            }
        }


        /// <summary>
        /// 更新地图
        /// </summary>
        internal void UpdateMap()
        {
            if (ClientRectangle.Width * ClientRectangle.Height == 0)//如果窗体最小化则不绘制
                return;
            View.UpdateRectangle(ClientRectangle);//更新当前View的窗口尺寸
            if (BackWindow != null)//根据最新的地图窗口尺寸建立背景窗口
                BackWindow.Dispose();
            BackWindow = new Bitmap(ClientRectangle.Width, ClientRectangle.Height);
            Graphics g = Graphics.FromImage(BackWindow);//在背景窗口上绘图
            g.FillRectangle(new SolidBrush(Color.Black), ClientRectangle);
            Layer.draw(g, View);
            Graphics graphics = CreateGraphics();//把背景窗口内容复制到到前景窗口上
            graphics.DrawImage(BackWindow, 0, 0);
            UpdateStatusBar();
        }


        private void MapButtonClick(object sender, EventArgs e)
        {
            GISMapActions actions = GISMapActions.zoomin;
            if ((Button)sender == ZoomInButton) actions = GISMapActions.zoomin;
            else if ((Button)sender == ZoomOutButton) actions = GISMapActions.zoomout;
            else if ((Button)sender == MoveDownButton) actions = GISMapActions.movedown;
            else if ((Button)sender == MoveUpButton) actions = GISMapActions.moveup;
            else if ((Button)sender == MoveLeftButton) actions = GISMapActions.moveleft;
            else if ((Button)sender == MoveRightButton) actions = GISMapActions.moveright;
            View.ChangeView(actions);
            UpdateMap();
        }

        /// <summary>
        /// 更改缩放系数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoomingFactorBox_ValueChanged(object sender, EventArgs e)
        {
            Layer.Extent.ZoomingFactor = (double)ZoomingFactorBox.Value;
        }
        private void MovingFactorBox_ValueChanged(object sender, EventArgs e)
        {
            Layer.Extent.MovingFactor = (double)MovingFactorBox.Value;
        }



        /// <summary>
        /// 打开属性表按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenTableButton_Click(object sender, EventArgs e)
        {
            OpenAttributeWindow();
        }
        private void OpenAttributeWindow()
        {
            if (Layer == null)
                return;
            if (AttributeWindow == null)
                AttributeWindow = new ShowAttribute(Layer, this);
            if (AttributeWindow.IsDisposed)//若属性窗口资源被释放
                AttributeWindow = new ShowAttribute(Layer, this);
            AttributeWindow.Show();
            if (AttributeWindow.WindowState == FormWindowState.Minimized)//若属性窗口最小化
                AttributeWindow.WindowState = FormWindowState.Normal;
            AttributeWindow.BringToFront();//把属性窗口放到最前端展示
        }



        private void SaveFileButton_Click(object sender, EventArgs e)
        {
            WriteFile(Layer, @"D:\VS2019\MyGISFile\mygisfile.rcx");
            MessageBox.Show("done");
        }

        private void ReadFileButton_Click(object sender, EventArgs e)
        {
            Layer = ReadFile(@"D:\VS2019\MyGISFile\mygisfile.rcx");
            MessageBox.Show("read" + Layer.FeatureCount() + "objects");
            View.UpdateExtent(Layer.Extent);
            UpdateMap();
        }


        /// <summary>
        /// 点选空间对象
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (Layer == null) return;
            GISVertex v = View.ToMapVertex(new Point(e.X, e.Y));
            SelectResult sr = Layer.Select(v, View);
            GISSelect gs = new GISSelect();
            gs.Select(v, Layer.GetAllFeatures(), Layer.ShapeType, View);
            if (sr == SelectResult.OK)//if(选中空间对象)
            {
                if (Layer.ShapeType == GISShapefile.SHAPETYPE.poloygon)
                    //MessageBox.Show(gs.SelectedFeatures[0].GetAttribute(0).ToString());
                    MessageBox.Show(gs.SelectedFeatures[0].GetAttribute(0).ToString()+ "\nx:" + gs.SelectedFeatures[0].spatialpart.centroid.x.ToString() +
                        " y:" + gs.SelectedFeatures[0].spatialpart.centroid.y.ToString());
                else
                    //MessageBox.Show(gs.SelectedFeature.GetAttribute(0).ToString());//显示第一字段属性值
                    MessageBox.Show(gs.SelectedFeature.GetAttribute(0).ToString()+"\nx:" + gs.SelectedFeature.spatialpart.centroid.x.ToString() + 
                        " y:" + gs.SelectedFeature.spatialpart.centroid.y.ToString());
                UpdateMap();
                UpdateAttributeWindow();
            }

        }

        private void ClearSelectionButton_Click(object sender, EventArgs e)
        {
            if (Layer == null)
                return;
            Layer.ClearSelection();
            UpdateMap();
            SelectedFeaturesCount.Text = "已选中的空间对象的个数为0";
            UpdateAttributeWindow();
        }


        /// <summary>
        /// 使地图大小适应窗口大小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Resize(object sender, EventArgs e)//存在bug：更改窗口大小时还需要点击“显示全图”
        {
            if (Layer != null)
            {
                UpdateMap();
            }
        }



        /// <summary>
        /// 更新属性表窗口
        /// </summary>
        private void UpdateAttributeWindow()
        {
            if (Layer == null)
                return;
            if (AttributeWindow == null)
                return;
            if (AttributeWindow.IsDisposed)//属性窗口资源已被释放
                return;
            AttributeWindow.UpdateData();
        }


        /// <summary>
        /// 更新状态栏信息
        /// </summary>
        public void UpdateStatusBar()
        {
            SelectedFeaturesCount.Text = "已选中的空间对象的个数为"+Layer.Selection.Count.ToString();
        }



        /// <summary>
        /// 重绘窗体，解决最小化后内容消失的问题
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            if (View != null)
            {
                if (BackWindow != null)
                    e.Graphics.DrawImage(BackWindow, 0, 0);
            }
        }


        public GISLayer GetGISLayer()
        {
            return Layer;
        }

        private void ConvertToTextButton_Click(object sender, EventArgs e)
        {
           
            using (var saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "文本文件(*.txt)|*.txt";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = saveFileDialog.FileName;
                    try
                    {
                        ShapefileConverter.ConvertToText(Layer,ShapefilePath, filePath);
                        MessageBox.Show("文件保存成功");
                    }
                    catch(Exception error)
                    {
                        MessageBox.Show("文件保存失败\n" + error.Message);
                    }
                }
            }
        }


        private void ReadTxtButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt 文件|*.txt";//文件类型限制为txt
            openFileDialog.RestoreDirectory = false;//不还原选择文件时更改的目录
            openFileDialog.FilterIndex = 1;
            openFileDialog.Multiselect = false;//不允许同时选中多个文件
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;
            TxtFilePath = openFileDialog.FileName;
            Layer = ShapefileConverter.ReadTxt(TxtFilePath);
            Layer.DrawAttributeOrNot = false;
            MessageBox.Show("read" + Layer.FeatureCount() + "objects.");
            View.UpdateExtent(Layer.Extent);
            UpdateMap();
        }
    }
}
