﻿
namespace Midterm_test
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.OpenFileButton = new System.Windows.Forms.Button();
            this.ZoomOutButton = new System.Windows.Forms.Button();
            this.ZoomInButton = new System.Windows.Forms.Button();
            this.ShowMapButton = new System.Windows.Forms.Button();
            this.MoveLeftButton = new System.Windows.Forms.Button();
            this.MoveDownButton = new System.Windows.Forms.Button();
            this.MoveUpButton = new System.Windows.Forms.Button();
            this.MoveRightButton = new System.Windows.Forms.Button();
            this.ZoomingFactorBox = new System.Windows.Forms.NumericUpDown();
            this.MovingFactorBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.OpenTableButton = new System.Windows.Forms.Button();
            this.SaveFileButton = new System.Windows.Forms.Button();
            this.ReadFileButton = new System.Windows.Forms.Button();
            this.ClearSelectionButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.SelectedFeaturesCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.ConvertToTextButton = new System.Windows.Forms.Button();
            this.ReadTxtButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ZoomingFactorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MovingFactorBox)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OpenFileButton
            // 
            this.OpenFileButton.Location = new System.Drawing.Point(21, 30);
            this.OpenFileButton.Name = "OpenFileButton";
            this.OpenFileButton.Size = new System.Drawing.Size(203, 76);
            this.OpenFileButton.TabIndex = 0;
            this.OpenFileButton.Text = "打开Shapefile文件";
            this.OpenFileButton.UseVisualStyleBackColor = true;
            this.OpenFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
            // 
            // ZoomOutButton
            // 
            this.ZoomOutButton.Location = new System.Drawing.Point(521, 30);
            this.ZoomOutButton.Name = "ZoomOutButton";
            this.ZoomOutButton.Size = new System.Drawing.Size(160, 76);
            this.ZoomOutButton.TabIndex = 2;
            this.ZoomOutButton.Text = "缩小";
            this.ZoomOutButton.UseVisualStyleBackColor = true;
            this.ZoomOutButton.Click += new System.EventHandler(this.MapButtonClick);
            // 
            // ZoomInButton
            // 
            this.ZoomInButton.Location = new System.Drawing.Point(374, 30);
            this.ZoomInButton.Name = "ZoomInButton";
            this.ZoomInButton.Size = new System.Drawing.Size(160, 76);
            this.ZoomInButton.TabIndex = 3;
            this.ZoomInButton.Text = "放大";
            this.ZoomInButton.UseVisualStyleBackColor = true;
            this.ZoomInButton.Click += new System.EventHandler(this.MapButtonClick);
            // 
            // ShowMapButton
            // 
            this.ShowMapButton.Location = new System.Drawing.Point(213, 30);
            this.ShowMapButton.Name = "ShowMapButton";
            this.ShowMapButton.Size = new System.Drawing.Size(121, 76);
            this.ShowMapButton.TabIndex = 4;
            this.ShowMapButton.Text = "显示全图";
            this.ShowMapButton.UseVisualStyleBackColor = true;
            this.ShowMapButton.Click += new System.EventHandler(this.ShowMapButton_Click);
            // 
            // MoveLeftButton
            // 
            this.MoveLeftButton.Location = new System.Drawing.Point(767, 72);
            this.MoveLeftButton.Name = "MoveLeftButton";
            this.MoveLeftButton.Size = new System.Drawing.Size(160, 76);
            this.MoveLeftButton.TabIndex = 5;
            this.MoveLeftButton.Text = "左移";
            this.MoveLeftButton.UseVisualStyleBackColor = true;
            this.MoveLeftButton.Click += new System.EventHandler(this.MapButtonClick);
            // 
            // MoveDownButton
            // 
            this.MoveDownButton.Location = new System.Drawing.Point(859, 154);
            this.MoveDownButton.Name = "MoveDownButton";
            this.MoveDownButton.Size = new System.Drawing.Size(160, 76);
            this.MoveDownButton.TabIndex = 6;
            this.MoveDownButton.Text = "下移";
            this.MoveDownButton.UseVisualStyleBackColor = true;
            this.MoveDownButton.Click += new System.EventHandler(this.MapButtonClick);
            // 
            // MoveUpButton
            // 
            this.MoveUpButton.Location = new System.Drawing.Point(859, -9);
            this.MoveUpButton.Name = "MoveUpButton";
            this.MoveUpButton.Size = new System.Drawing.Size(160, 76);
            this.MoveUpButton.TabIndex = 7;
            this.MoveUpButton.Text = "上移";
            this.MoveUpButton.UseVisualStyleBackColor = true;
            this.MoveUpButton.Click += new System.EventHandler(this.MapButtonClick);
            // 
            // MoveRightButton
            // 
            this.MoveRightButton.Location = new System.Drawing.Point(933, 73);
            this.MoveRightButton.Name = "MoveRightButton";
            this.MoveRightButton.Size = new System.Drawing.Size(160, 76);
            this.MoveRightButton.TabIndex = 9;
            this.MoveRightButton.Text = "右移";
            this.MoveRightButton.UseVisualStyleBackColor = true;
            this.MoveRightButton.Click += new System.EventHandler(this.MapButtonClick);
            // 
            // ZoomingFactorBox
            // 
            this.ZoomingFactorBox.DecimalPlaces = 2;
            this.ZoomingFactorBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.ZoomingFactorBox.Location = new System.Drawing.Point(374, 117);
            this.ZoomingFactorBox.Name = "ZoomingFactorBox";
            this.ZoomingFactorBox.Size = new System.Drawing.Size(120, 31);
            this.ZoomingFactorBox.TabIndex = 10;
            this.ZoomingFactorBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.ZoomingFactorBox.ValueChanged += new System.EventHandler(this.ZoomingFactorBox_ValueChanged);
            // 
            // MovingFactorBox
            // 
            this.MovingFactorBox.DecimalPlaces = 2;
            this.MovingFactorBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.MovingFactorBox.Location = new System.Drawing.Point(641, 117);
            this.MovingFactorBox.Name = "MovingFactorBox";
            this.MovingFactorBox.Size = new System.Drawing.Size(120, 31);
            this.MovingFactorBox.TabIndex = 11;
            this.MovingFactorBox.Value = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.MovingFactorBox.ValueChanged += new System.EventHandler(this.MovingFactorBox_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(274, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 12;
            this.label1.Text = "缩放系数";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(541, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 13;
            this.label2.Text = "移动系数";
            // 
            // OpenTableButton
            // 
            this.OpenTableButton.Location = new System.Drawing.Point(1168, 48);
            this.OpenTableButton.Name = "OpenTableButton";
            this.OpenTableButton.Size = new System.Drawing.Size(144, 58);
            this.OpenTableButton.TabIndex = 14;
            this.OpenTableButton.Text = "打开属性表";
            this.OpenTableButton.UseVisualStyleBackColor = true;
            this.OpenTableButton.Click += new System.EventHandler(this.OpenTableButton_Click);
            // 
            // SaveFileButton
            // 
            this.SaveFileButton.Location = new System.Drawing.Point(1168, 102);
            this.SaveFileButton.Name = "SaveFileButton";
            this.SaveFileButton.Size = new System.Drawing.Size(144, 59);
            this.SaveFileButton.TabIndex = 15;
            this.SaveFileButton.Text = "存储文件";
            this.SaveFileButton.UseVisualStyleBackColor = true;
            this.SaveFileButton.Click += new System.EventHandler(this.SaveFileButton_Click);
            // 
            // ReadFileButton
            // 
            this.ReadFileButton.Location = new System.Drawing.Point(1168, 154);
            this.ReadFileButton.Name = "ReadFileButton";
            this.ReadFileButton.Size = new System.Drawing.Size(144, 59);
            this.ReadFileButton.TabIndex = 16;
            this.ReadFileButton.Text = "读取文件";
            this.ReadFileButton.UseVisualStyleBackColor = true;
            this.ReadFileButton.Click += new System.EventHandler(this.ReadFileButton_Click);
            // 
            // ClearSelectionButton
            // 
            this.ClearSelectionButton.Location = new System.Drawing.Point(21, 112);
            this.ClearSelectionButton.Name = "ClearSelectionButton";
            this.ClearSelectionButton.Size = new System.Drawing.Size(134, 60);
            this.ClearSelectionButton.TabIndex = 17;
            this.ClearSelectionButton.Text = "清空选择";
            this.ClearSelectionButton.UseVisualStyleBackColor = true;
            this.ClearSelectionButton.Click += new System.EventHandler(this.ClearSelectionButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SelectedFeaturesCount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 876);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1563, 22);
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // SelectedFeaturesCount
            // 
            this.SelectedFeaturesCount.Name = "SelectedFeaturesCount";
            this.SelectedFeaturesCount.Size = new System.Drawing.Size(0, 13);
            // 
            // ConvertToTextButton
            // 
            this.ConvertToTextButton.Location = new System.Drawing.Point(1382, 57);
            this.ConvertToTextButton.Name = "ConvertToTextButton";
            this.ConvertToTextButton.Size = new System.Drawing.Size(140, 64);
            this.ConvertToTextButton.TabIndex = 19;
            this.ConvertToTextButton.Text = "转为txt文件";
            this.ConvertToTextButton.UseVisualStyleBackColor = true;
            this.ConvertToTextButton.Click += new System.EventHandler(this.ConvertToTextButton_Click);
            // 
            // ReadTxtButton
            // 
            this.ReadTxtButton.Location = new System.Drawing.Point(1382, 119);
            this.ReadTxtButton.Name = "ReadTxtButton";
            this.ReadTxtButton.Size = new System.Drawing.Size(140, 64);
            this.ReadTxtButton.TabIndex = 20;
            this.ReadTxtButton.Text = "读取txt文件";
            this.ReadTxtButton.UseVisualStyleBackColor = true;
            this.ReadTxtButton.Click += new System.EventHandler(this.ReadTxtButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1563, 898);
            this.Controls.Add(this.ReadTxtButton);
            this.Controls.Add(this.ConvertToTextButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ClearSelectionButton);
            this.Controls.Add(this.ReadFileButton);
            this.Controls.Add(this.SaveFileButton);
            this.Controls.Add(this.OpenTableButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MovingFactorBox);
            this.Controls.Add(this.ZoomingFactorBox);
            this.Controls.Add(this.MoveRightButton);
            this.Controls.Add(this.MoveUpButton);
            this.Controls.Add(this.MoveDownButton);
            this.Controls.Add(this.MoveLeftButton);
            this.Controls.Add(this.ShowMapButton);
            this.Controls.Add(this.ZoomInButton);
            this.Controls.Add(this.ZoomOutButton);
            this.Controls.Add(this.OpenFileButton);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ZoomingFactorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MovingFactorBox)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OpenFileButton;
        private System.Windows.Forms.Button ZoomOutButton;
        private System.Windows.Forms.Button ZoomInButton;
        private System.Windows.Forms.Button ShowMapButton;
        private System.Windows.Forms.Button MoveLeftButton;
        private System.Windows.Forms.Button MoveDownButton;
        private System.Windows.Forms.Button MoveUpButton;
        private System.Windows.Forms.Button MoveRightButton;
        private System.Windows.Forms.NumericUpDown ZoomingFactorBox;
        private System.Windows.Forms.NumericUpDown MovingFactorBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OpenTableButton;
        private System.Windows.Forms.Button SaveFileButton;
        private System.Windows.Forms.Button ReadFileButton;
        private System.Windows.Forms.Button ClearSelectionButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel SelectedFeaturesCount;
        private System.Windows.Forms.Button ConvertToTextButton;
        private System.Windows.Forms.Button ReadTxtButton;
    }
}

