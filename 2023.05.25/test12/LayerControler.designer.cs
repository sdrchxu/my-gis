﻿
namespace test12
{
    partial class LayerControler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LayerListBox = new System.Windows.Forms.ListBox();
            this.SelectableCheckBox = new System.Windows.Forms.CheckBox();
            this.VisibleCheckBox = new System.Windows.Forms.CheckBox();
            this.DrawAttCheckBox = new System.Windows.Forms.CheckBox();
            this.FieldComboBox = new System.Windows.Forms.ComboBox();
            this.fileAddresslabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LayerNameTextBox = new System.Windows.Forms.TextBox();
            this.AddLayerButton = new System.Windows.Forms.Button();
            this.DeleteLayerButton = new System.Windows.Forms.Button();
            this.ExportLayerButton = new System.Windows.Forms.Button();
            this.SaveFileButton = new System.Windows.Forms.Button();
            this.MoveUpButton = new System.Windows.Forms.Button();
            this.MoveDownButton = new System.Windows.Forms.Button();
            this.OpenTableButton = new System.Windows.Forms.Button();
            this.ModifyButton = new System.Windows.Forms.Button();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LayerListBox
            // 
            this.LayerListBox.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LayerListBox.FormattingEnabled = true;
            this.LayerListBox.ItemHeight = 25;
            this.LayerListBox.Location = new System.Drawing.Point(29, 47);
            this.LayerListBox.Margin = new System.Windows.Forms.Padding(4);
            this.LayerListBox.Name = "LayerListBox";
            this.LayerListBox.Size = new System.Drawing.Size(1969, 829);
            this.LayerListBox.TabIndex = 0;
            this.LayerListBox.SelectedIndexChanged += new System.EventHandler(this.LayerListBox_SelectedIndexChanged);
            // 
            // SelectableCheckBox
            // 
            this.SelectableCheckBox.AutoSize = true;
            this.SelectableCheckBox.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SelectableCheckBox.Location = new System.Drawing.Point(62, 1064);
            this.SelectableCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.SelectableCheckBox.Name = "SelectableCheckBox";
            this.SelectableCheckBox.Size = new System.Drawing.Size(116, 30);
            this.SelectableCheckBox.TabIndex = 1;
            this.SelectableCheckBox.Text = "可选择";
            this.SelectableCheckBox.UseVisualStyleBackColor = true;
            this.SelectableCheckBox.Click += new System.EventHandler(this.Clicked);
            // 
            // VisibleCheckBox
            // 
            this.VisibleCheckBox.AutoSize = true;
            this.VisibleCheckBox.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.VisibleCheckBox.Location = new System.Drawing.Point(62, 1130);
            this.VisibleCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.VisibleCheckBox.Name = "VisibleCheckBox";
            this.VisibleCheckBox.Size = new System.Drawing.Size(90, 30);
            this.VisibleCheckBox.TabIndex = 2;
            this.VisibleCheckBox.Text = "可视";
            this.VisibleCheckBox.UseVisualStyleBackColor = true;
            this.VisibleCheckBox.Click += new System.EventHandler(this.Clicked);
            // 
            // DrawAttCheckBox
            // 
            this.DrawAttCheckBox.AutoSize = true;
            this.DrawAttCheckBox.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DrawAttCheckBox.Location = new System.Drawing.Point(62, 1200);
            this.DrawAttCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.DrawAttCheckBox.Name = "DrawAttCheckBox";
            this.DrawAttCheckBox.Size = new System.Drawing.Size(142, 30);
            this.DrawAttCheckBox.TabIndex = 3;
            this.DrawAttCheckBox.Text = "自动标注";
            this.DrawAttCheckBox.UseVisualStyleBackColor = true;
            this.DrawAttCheckBox.Click += new System.EventHandler(this.Clicked);
            // 
            // FieldComboBox
            // 
            this.FieldComboBox.FormattingEnabled = true;
            this.FieldComboBox.Location = new System.Drawing.Point(422, 1218);
            this.FieldComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.FieldComboBox.Name = "FieldComboBox";
            this.FieldComboBox.Size = new System.Drawing.Size(352, 29);
            this.FieldComboBox.TabIndex = 4;
            this.FieldComboBox.Click += new System.EventHandler(this.Clicked);
            // 
            // fileAddresslabel
            // 
            this.fileAddresslabel.AutoSize = true;
            this.fileAddresslabel.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.fileAddresslabel.Location = new System.Drawing.Point(517, 1071);
            this.fileAddresslabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fileAddresslabel.Name = "fileAddresslabel";
            this.fileAddresslabel.Size = new System.Drawing.Size(142, 26);
            this.fileAddresslabel.TabIndex = 5;
            this.fileAddresslabel.Text = "文件地址：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(517, 1152);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 26);
            this.label2.TabIndex = 6;
            this.label2.Text = "图层名称：";
            // 
            // LayerNameTextBox
            // 
            this.LayerNameTextBox.Location = new System.Drawing.Point(853, 1146);
            this.LayerNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.LayerNameTextBox.Name = "LayerNameTextBox";
            this.LayerNameTextBox.Size = new System.Drawing.Size(371, 31);
            this.LayerNameTextBox.TabIndex = 7;
            // 
            // AddLayerButton
            // 
            this.AddLayerButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.AddLayerButton.Location = new System.Drawing.Point(884, 914);
            this.AddLayerButton.Margin = new System.Windows.Forms.Padding(4);
            this.AddLayerButton.Name = "AddLayerButton";
            this.AddLayerButton.Size = new System.Drawing.Size(259, 94);
            this.AddLayerButton.TabIndex = 8;
            this.AddLayerButton.Text = "添加图层";
            this.AddLayerButton.UseVisualStyleBackColor = true;
            this.AddLayerButton.Click += new System.EventHandler(this.AddLayerButton_Click);
            // 
            // DeleteLayerButton
            // 
            this.DeleteLayerButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DeleteLayerButton.Location = new System.Drawing.Point(1150, 914);
            this.DeleteLayerButton.Margin = new System.Windows.Forms.Padding(4);
            this.DeleteLayerButton.Name = "DeleteLayerButton";
            this.DeleteLayerButton.Size = new System.Drawing.Size(251, 94);
            this.DeleteLayerButton.TabIndex = 9;
            this.DeleteLayerButton.Text = "删除图层";
            this.DeleteLayerButton.UseVisualStyleBackColor = true;
            this.DeleteLayerButton.Click += new System.EventHandler(this.DeleteLayerButton_Click);
            // 
            // ExportLayerButton
            // 
            this.ExportLayerButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ExportLayerButton.Location = new System.Drawing.Point(1408, 914);
            this.ExportLayerButton.Margin = new System.Windows.Forms.Padding(4);
            this.ExportLayerButton.Name = "ExportLayerButton";
            this.ExportLayerButton.Size = new System.Drawing.Size(277, 94);
            this.ExportLayerButton.TabIndex = 10;
            this.ExportLayerButton.Text = "导出图层";
            this.ExportLayerButton.UseVisualStyleBackColor = true;
            this.ExportLayerButton.Click += new System.EventHandler(this.ExportLayerButton_Click);
            // 
            // SaveFileButton
            // 
            this.SaveFileButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SaveFileButton.Location = new System.Drawing.Point(1692, 914);
            this.SaveFileButton.Margin = new System.Windows.Forms.Padding(4);
            this.SaveFileButton.Name = "SaveFileButton";
            this.SaveFileButton.Size = new System.Drawing.Size(244, 94);
            this.SaveFileButton.TabIndex = 11;
            this.SaveFileButton.Text = "存储文档";
            this.SaveFileButton.UseVisualStyleBackColor = true;
            this.SaveFileButton.Click += new System.EventHandler(this.SaveFileButton_Click);
            // 
            // MoveUpButton
            // 
            this.MoveUpButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MoveUpButton.Location = new System.Drawing.Point(2061, 234);
            this.MoveUpButton.Margin = new System.Windows.Forms.Padding(4);
            this.MoveUpButton.Name = "MoveUpButton";
            this.MoveUpButton.Size = new System.Drawing.Size(182, 93);
            this.MoveUpButton.TabIndex = 12;
            this.MoveUpButton.Text = "上移";
            this.MoveUpButton.UseVisualStyleBackColor = true;
            this.MoveUpButton.Click += new System.EventHandler(this.MoveUpButton_Click);
            // 
            // MoveDownButton
            // 
            this.MoveDownButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MoveDownButton.Location = new System.Drawing.Point(2061, 334);
            this.MoveDownButton.Margin = new System.Windows.Forms.Padding(4);
            this.MoveDownButton.Name = "MoveDownButton";
            this.MoveDownButton.Size = new System.Drawing.Size(182, 77);
            this.MoveDownButton.TabIndex = 13;
            this.MoveDownButton.Text = "下移";
            this.MoveDownButton.UseVisualStyleBackColor = true;
            this.MoveDownButton.Click += new System.EventHandler(this.MoveDownButton_Click);
            // 
            // OpenTableButton
            // 
            this.OpenTableButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.OpenTableButton.Location = new System.Drawing.Point(2009, 500);
            this.OpenTableButton.Margin = new System.Windows.Forms.Padding(4);
            this.OpenTableButton.Name = "OpenTableButton";
            this.OpenTableButton.Size = new System.Drawing.Size(325, 96);
            this.OpenTableButton.TabIndex = 14;
            this.OpenTableButton.Text = "打开属性表";
            this.OpenTableButton.UseVisualStyleBackColor = true;
            this.OpenTableButton.Click += new System.EventHandler(this.OpenTableButton_Click);
            // 
            // ModifyButton
            // 
            this.ModifyButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ModifyButton.Location = new System.Drawing.Point(1234, 1136);
            this.ModifyButton.Margin = new System.Windows.Forms.Padding(4);
            this.ModifyButton.Name = "ModifyButton";
            this.ModifyButton.Size = new System.Drawing.Size(176, 61);
            this.ModifyButton.TabIndex = 15;
            this.ModifyButton.Text = "修改";
            this.ModifyButton.UseVisualStyleBackColor = true;
            this.ModifyButton.Click += new System.EventHandler(this.ModifyButton_Click);
            // 
            // ApplyButton
            // 
            this.ApplyButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ApplyButton.Location = new System.Drawing.Point(1954, 1292);
            this.ApplyButton.Margin = new System.Windows.Forms.Padding(4);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(205, 82);
            this.ApplyButton.TabIndex = 16;
            this.ApplyButton.Text = "应用";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Font = new System.Drawing.Font("宋体", 10.71429F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CloseButton.Location = new System.Drawing.Point(2176, 1292);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(4);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(205, 82);
            this.CloseButton.TabIndex = 17;
            this.CloseButton.Text = "关闭";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // LayerControler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2440, 1393);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.ModifyButton);
            this.Controls.Add(this.OpenTableButton);
            this.Controls.Add(this.MoveDownButton);
            this.Controls.Add(this.MoveUpButton);
            this.Controls.Add(this.SaveFileButton);
            this.Controls.Add(this.ExportLayerButton);
            this.Controls.Add(this.DeleteLayerButton);
            this.Controls.Add(this.AddLayerButton);
            this.Controls.Add(this.LayerNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fileAddresslabel);
            this.Controls.Add(this.FieldComboBox);
            this.Controls.Add(this.DrawAttCheckBox);
            this.Controls.Add(this.VisibleCheckBox);
            this.Controls.Add(this.SelectableCheckBox);
            this.Controls.Add(this.LayerListBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LayerControler";
            this.Text = "图层管理器";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.LayerControler_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LayerListBox;
        private System.Windows.Forms.CheckBox SelectableCheckBox;
        private System.Windows.Forms.CheckBox VisibleCheckBox;
        private System.Windows.Forms.CheckBox DrawAttCheckBox;
        private System.Windows.Forms.ComboBox FieldComboBox;
        private System.Windows.Forms.Label fileAddresslabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LayerNameTextBox;
        private System.Windows.Forms.Button AddLayerButton;
        private System.Windows.Forms.Button DeleteLayerButton;
        private System.Windows.Forms.Button ExportLayerButton;
        private System.Windows.Forms.Button SaveFileButton;
        private System.Windows.Forms.Button MoveUpButton;
        private System.Windows.Forms.Button MoveDownButton;
        private System.Windows.Forms.Button OpenTableButton;
        private System.Windows.Forms.Button ModifyButton;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CloseButton;
    }
}