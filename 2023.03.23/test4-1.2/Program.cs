﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test4_1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Equivalence<int> equ = new Equivalence<int>((x, y) => x == y);//比较输入的两个整数是否相等
            //测试EquivalentTo(T x,T y):
            Console.WriteLine(equ.EquivalentTo(3, 300));
            Console.WriteLine(equ.EquivalentTo(10, 10));
            //测试GetEquivalentList(T[] ts, T key):
            int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] k = { 1, 2 };
            int[] eqList = equ.GetEquivalentList(arr, k);
            if (eqList.Length == 0)
            {
                Console.WriteLine("没找到符合条件的值");
            }
            for (int i = 0; i < eqList.Length; i++)
            {
                Console.Write("{0}  ", eqList[i]);
            }
            Console.WriteLine();
        }
    }

    public class Equivalence<T>
    {
        private Func<T, T, bool> cfun;
        public Equivalence(Func<T, T, bool> cfun)
        {
            this.cfun = cfun;
        }
        public bool EquivalentTo(T x, T y)//判断两个对象是否等价，调用cfun封装的方法进行判断
        {
            return cfun(x, y);
        }
        public T[] GetEquivalentList(T[] ts, T[] key)//改进：T型改为T[]，可以同时指定多个查找对象
        {
            List<T> result = new List<T>();
            for (int i = 0; i < ts.Length; i++)
            {
                for (int j = 0; j < key.Length; j++)
                {
                    if (EquivalentTo(ts[i], key[j]))
                    {
                        result.Add(ts[i]);
                    }
                }
            }
            return result.ToArray();
        }

    }
}
