﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyGIS;

namespace test4_2
{

    public partial class Form1 : Form
    {
        GISView view = null;
        List<GISFeature> features = new List<GISFeature>();
        public Form1()
        {
            InitializeComponent();
            view = new GISView(new GISExtent(new GISVertex(0, 0), new GISVertex(100, 100)), ClientRectangle);

        }

        private void AddPoint_Click(object sender, EventArgs e)//添加点操作
        {
            double x = Convert.ToDouble(XInput.Text);
            double y = Convert.ToDouble(YInput.Text);
            GISVertex onevertex = new GISVertex(x, y);
            GISPoint onepoint = new GISPoint(onevertex);
            string attribute = AttInput.Text;
            GISAttribute oneattribute = new GISAttribute();
            oneattribute.AddValue(attribute);
            GISFeature onefeature = new GISFeature(onepoint, oneattribute);
            features.Add(onefeature);
            Graphics graphics = this.CreateGraphics();
            onefeature.draw(graphics, view, true, 0);
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)//根据鼠标点击位置查找属性
        {
            GISVertex mouselocation = view.ToMapVertex(new Point(e.X, e.Y));//将鼠标的点击位置转换为地图坐标
            double mindistance = double.MaxValue;
            int findid = -1;
            for (int i = 0; i < features.Count; i++)
            {
                double onedistance = features[i].spatialpart.centroid.Distance(mouselocation);//计算鼠标点击位置到空间要素中心点的距离
                if (onedistance < mindistance)
                {
                    mindistance = onedistance;
                    findid = i;
                }
            }
            if (findid == -1)
            {
                MessageBox.Show("没有任何空间对象！");
                return;
            }

            Point nearestpoint = view.ToScreenPoint(features[findid].spatialpart.centroid);//将最近的空间中心点转换为屏幕坐标
            int screendistance = Math.Abs(nearestpoint.X - e.X) + Math.Abs(nearestpoint.Y - e.Y);//Math.Abs表示取得绝对值
            if (screendistance > 5)
            {
                MessageBox.Show("请靠近空间对象点击！");
                return;
            }
            MessageBox.Show("该空间对象的属性是" + features[findid].getAttribute(0));
        }

        private void UpdateMap_MouseClick(object sender, MouseEventArgs e)//更新地图按钮
        {
            double minx = Double.Parse(MinXInput.Text);
            double miny = Double.Parse(MinYInput.Text);
            double maxx = Double.Parse(MaxXInput.Text);
            double maxy = Double.Parse(MaxYInput.Text);
            view.UpdateMap(new GISExtent(new GISVertex(minx, miny), new GISVertex(maxx, maxy)), ClientRectangle);
            UpdateMap();
        }

        private void MapButtonClick(object sender, EventArgs e)
        {
            GISMapActions action = GISMapActions.zoomin;
            if ((Button)sender == ZoomInButton) action = GISMapActions.zoomin;
            else if ((Button)sender == ZoomOutButton) action = GISMapActions.zoomout;
            else if ((Button)sender == MoveDownButton) action = GISMapActions.movedown;
            else if ((Button)sender == MoveUpButton) action = GISMapActions.moveup;
            else if ((Button)sender == MoveRightButton) action = GISMapActions.moveright;
            else if ((Button)sender == MoveLeftButton) action = GISMapActions.moveleft;
            view.ChangeView(action);
            UpdateMap();
        }//实现地图平移与缩放操作，可以将多个按钮的事件绑定到此

        private void UpdateMap()//更新地图
        {
            Graphics graphics = CreateGraphics();
            graphics.FillRectangle(new SolidBrush(Color.Black), ClientRectangle);
            for (int i = 0; i < features.Count; i++)
            {
                features[i].draw(graphics, view, true, 0);
            }
        }
    }
}
