﻿using MyGIS;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace test_1_2._2
{
    public partial class Form1 : Form
    {
        List<GISPoint> points = new List<GISPoint>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }



        private void CreVertex_Click(object sender, EventArgs e)
        {
            double x = Convert.ToDouble(XInput.Text);
            double y = Convert.ToDouble(YInput.Text);
            string attribute = AttInput.Text;
            GISVertex onevertex = new GISVertex(x, y);
            GISPoint onepoint = new GISPoint(onevertex, attribute);
            Graphics graphics = this.CreateGraphics();
            onepoint.DrawPoint(graphics);
            onepoint.DrawAttribute(graphics);
            points.Add(onepoint);
        }//画一个点

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            GISVertex onevertex = new GISVertex((double)e.X, (double)e.Y);
            double mindistance = Double.MaxValue;
            int findid = -1;
            for (int i = 0; i < points.Count; i++)
            {
                double distance = points[i].Distance(onevertex);
                if (distance < mindistance)
                {
                    mindistance = distance;
                    findid = i;
                }
            }
            if (mindistance > 5 || findid == -1)
            {
                MessageBox.Show("没有点实体或鼠标点击位置不准确");
            }
            else
            {
                MessageBox.Show(points[findid].Attribute);
            }
        }//根据鼠标点击位置查询点的属性

        private void CreateRadom_Click(object sender, EventArgs e)
        {
            int a = (int)RadomCount.Value;
            for(int i = 0; i < a; i++)
            {
                
                byte[] buffer = Guid.NewGuid().ToByteArray();
                int iSeed = BitConverter.ToInt32(buffer, 0);
                Random random1 = new Random(iSeed);
                int x1=random1.Next(100,800);
                byte[] buffer2 = Guid.NewGuid().ToByteArray();
                int iSeed2 = BitConverter.ToInt32(buffer2, 0);
                Random random2 = new Random(iSeed2);
                int y1 = random1.Next(100, 800);
                string attribute = "";
                GISVertex onevertex = new GISVertex(x1, y1);
                GISPoint onepoint = new GISPoint(onevertex, attribute);
                Graphics graphics = this.CreateGraphics();
                onepoint.DrawPoint(graphics);
            }
        }
    }
}
