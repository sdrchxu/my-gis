﻿
namespace test_1_2._2
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.CreVertex = new System.Windows.Forms.Button();
            this.XInput = new System.Windows.Forms.TextBox();
            this.YInput = new System.Windows.Forms.TextBox();
            this.AttInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CreateRadom = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.RadomCount = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.RadomCount)).BeginInit();
            this.SuspendLayout();
            // 
            // CreVertex
            // 
            this.CreVertex.Location = new System.Drawing.Point(1019, 52);
            this.CreVertex.Name = "CreVertex";
            this.CreVertex.Size = new System.Drawing.Size(157, 48);
            this.CreVertex.TabIndex = 0;
            this.CreVertex.Text = "添加点实体";
            this.CreVertex.UseVisualStyleBackColor = true;
            this.CreVertex.Click += new System.EventHandler(this.CreVertex_Click);
            // 
            // XInput
            // 
            this.XInput.Location = new System.Drawing.Point(112, 63);
            this.XInput.Name = "XInput";
            this.XInput.Size = new System.Drawing.Size(100, 31);
            this.XInput.TabIndex = 1;
            // 
            // YInput
            // 
            this.YInput.Location = new System.Drawing.Point(434, 63);
            this.YInput.Name = "YInput";
            this.YInput.Size = new System.Drawing.Size(100, 31);
            this.YInput.TabIndex = 2;
            // 
            // AttInput
            // 
            this.AttInput.Location = new System.Drawing.Point(712, 63);
            this.AttInput.Name = "AttInput";
            this.AttInput.Size = new System.Drawing.Size(100, 31);
            this.AttInput.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(637, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "属性";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(390, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 21);
            this.label3.TabIndex = 6;
            this.label3.Text = "Y";
            // 
            // CreateRadom
            // 
            this.CreateRadom.Location = new System.Drawing.Point(339, 123);
            this.CreateRadom.Name = "CreateRadom";
            this.CreateRadom.Size = new System.Drawing.Size(168, 42);
            this.CreateRadom.TabIndex = 8;
            this.CreateRadom.Text = "生成随机点";
            this.CreateRadom.UseVisualStyleBackColor = true;
            this.CreateRadom.Click += new System.EventHandler(this.CreateRadom_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 21);
            this.label4.TabIndex = 9;
            this.label4.Text = "生成随机点数量";
            // 
            // RadomCount
            // 
            this.RadomCount.Location = new System.Drawing.Point(188, 132);
            this.RadomCount.Name = "RadomCount";
            this.RadomCount.Size = new System.Drawing.Size(120, 31);
            this.RadomCount.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1202, 744);
            this.Controls.Add(this.RadomCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CreateRadom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AttInput);
            this.Controls.Add(this.YInput);
            this.Controls.Add(this.XInput);
            this.Controls.Add(this.CreVertex);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.RadomCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreVertex;
        private System.Windows.Forms.TextBox XInput;
        private System.Windows.Forms.TextBox YInput;
        private System.Windows.Forms.TextBox AttInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button CreateRadom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown RadomCount;
    }
}

