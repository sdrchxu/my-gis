﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MyGIS
{
    class GISVertex
    {
        public double x;
        public double y;
        public GISVertex(double _x, double _y)
        {
            x = _x;
            y = _y;
        }
        public double Distance(GISVertex anothervertex)
        {
            return Math.Sqrt((x - anothervertex.x) *
                (x - anothervertex.x) + (y - anothervertex.y) * (y - anothervertex.y));
        }//计算两点间距离
    }
    class GISPoint
    {
        public GISVertex Location;
        public string Attribute;//用来记录点实体的属性

        public GISPoint(GISVertex onevertex, string onestring)
        {
            Location = onevertex;
            Attribute = onestring;
        }//实例化点实体
        public void DrawPoint(Graphics graphics)
        {
            graphics.FillEllipse(new SolidBrush(Color.Red),
                new Rectangle((int)(Location.x) - 3, (int)(Location.y) - 3, 6, 6));
        }//画一个点实体
        public void DrawAttribute(Graphics graphics)
        {
            graphics.DrawString(Attribute, new Font("宋体", 20), new SolidBrush(Color.Green), new
                PointF((int)(Location.x), (int)(Location.y)));
        }
        public double Distance(GISVertex anothervertex)
        {
            return Location.Distance(anothervertex);
        }
    }
    class GISLine
    {
        List<GISVertex> AllVertexs;
    }
    class GISPolygon
    {
        List<GISVertex> AllVertexs;
    }
    //class GISFeature
    //{
    //    public GISSpatial spatialpart;
    //    public GISAttribute attributepart;
    //    public GISFeature(GISSpatial spatial,GISAttribute attribute)
    //    {
    //        spatialpart = spatial;
    //        attributepart = attribute;
    //    }
}

//class GISAttribute
//{
//    public Arraylist values = new Arraylist();
//}
//abstract class GISSpatial
//{
//    public GISVertex centroid;//空间实体的中心点
//    public GISExtent extent;//空间范围：最小外接矩形
//    public abstract void draw(Graphics graphics);
//}

//class GISExtent
//{
//    public GISVertex bottomleft;
//    public GISVertex upright;
//    public GISExtent(GISVertex _bottomleft,GISVertex _upright)
//    {
//        bottomleft = _bottomleft;
//        upright = _upright;
//    }
//}