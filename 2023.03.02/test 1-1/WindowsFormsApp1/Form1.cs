﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Calculator;//添加引用，使自定义的类可用

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        CCircle cir;
        CCone con;
        CCylinder cyl;
        public Form1()
        {
            InitializeComponent();
            cir = new CCircle() { r = 1 };
            con = new CCone() { c = cir, h = 1 };
            cyl = new CCylinder() { c = cir, h = 1 };
        }

        private void CalCircle_Click(object sender, EventArgs e)
        {
            cir.r = (double)NumCircle.Value;
            double a = cir.GetArea();
            MessageBox.Show("圆面积"+a);
        }

        private void CalCylinder_Click(object sender, EventArgs e)
        {
            cyl.h = (double)NumHigh.Value;
            double v = cyl.GetVolumn();
            MessageBox.Show("圆柱体体积" + v);

        }

        private void CalCone_Click(object sender, EventArgs e)
        {
            con.h = (double)NumHigh.Value;
            double v = con.GetVolumn();
            MessageBox.Show("圆锥体体积" + v);
        }
    }
}
