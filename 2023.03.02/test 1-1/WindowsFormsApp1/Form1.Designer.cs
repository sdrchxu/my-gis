﻿
namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.CalCircle = new System.Windows.Forms.Button();
            this.CalCylinder = new System.Windows.Forms.Button();
            this.NumCircle = new System.Windows.Forms.NumericUpDown();
            this.NumHigh = new System.Windows.Forms.NumericUpDown();
            this.CalCone = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NumCircle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumHigh)).BeginInit();
            this.SuspendLayout();
            // 
            // CalCircle
            // 
            this.CalCircle.Location = new System.Drawing.Point(528, 78);
            this.CalCircle.Name = "CalCircle";
            this.CalCircle.Size = new System.Drawing.Size(156, 70);
            this.CalCircle.TabIndex = 0;
            this.CalCircle.Text = "计算底面面积";
            this.CalCircle.UseVisualStyleBackColor = true;
            this.CalCircle.Click += new System.EventHandler(this.CalCircle_Click);
            // 
            // CalCylinder
            // 
            this.CalCylinder.Cursor = System.Windows.Forms.Cursors.Default;
            this.CalCylinder.Location = new System.Drawing.Point(429, 190);
            this.CalCylinder.Name = "CalCylinder";
            this.CalCylinder.Size = new System.Drawing.Size(150, 65);
            this.CalCylinder.TabIndex = 1;
            this.CalCylinder.Text = "计算圆柱面积";
            this.CalCylinder.UseVisualStyleBackColor = true;
            this.CalCylinder.Click += new System.EventHandler(this.CalCylinder_Click);
            // 
            // NumCircle
            // 
            this.NumCircle.Location = new System.Drawing.Point(258, 101);
            this.NumCircle.Name = "NumCircle";
            this.NumCircle.Size = new System.Drawing.Size(120, 31);
            this.NumCircle.TabIndex = 2;
            this.NumCircle.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // NumHigh
            // 
            this.NumHigh.Location = new System.Drawing.Point(258, 210);
            this.NumHigh.Name = "NumHigh";
            this.NumHigh.Size = new System.Drawing.Size(120, 31);
            this.NumHigh.TabIndex = 3;
            this.NumHigh.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // CalCone
            // 
            this.CalCone.Location = new System.Drawing.Point(624, 190);
            this.CalCone.Name = "CalCone";
            this.CalCone.Size = new System.Drawing.Size(149, 65);
            this.CalCone.TabIndex = 4;
            this.CalCone.Text = "计算圆锥面积";
            this.CalCone.UseVisualStyleBackColor = true;
            this.CalCone.Click += new System.EventHandler(this.CalCone_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "请输入底面半径：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 212);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "请输入高：";
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("宋体", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Title.Location = new System.Drawing.Point(100, 28);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(627, 34);
            this.Title.TabIndex = 7;
            this.Title.Text = "计算底面圆面积与圆柱体、圆锥体的体积";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 437);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CalCone);
            this.Controls.Add(this.NumHigh);
            this.Controls.Add(this.NumCircle);
            this.Controls.Add(this.CalCylinder);
            this.Controls.Add(this.CalCircle);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NumCircle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumHigh)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CalCircle;
        private System.Windows.Forms.Button CalCylinder;
        private System.Windows.Forms.NumericUpDown NumCircle;
        private System.Windows.Forms.NumericUpDown NumHigh;
        private System.Windows.Forms.Button CalCone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Title;
    }
}

