﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator //解算底面圆面积与圆柱、圆锥面积
{
    public class  CCircle
    {
        public double r;

        public double GetArea()
        {
            return r * r*3.14;
        }
    }

    public class CCone  //圆锥体
    {
        public CCircle c;
        public double h;
        public double GetVolumn()
        {
            double a=c.GetArea();
            return a * h / 3;
        }
    }


    public class CCylinder  //圆柱体
    {
        public CCircle c;
        public double h;
        public double GetVolumn()
        {
            return c.GetArea() * h;
        }
    }

}
