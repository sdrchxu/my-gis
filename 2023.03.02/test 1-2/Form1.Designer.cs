﻿
namespace test_1_2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.X = new System.Windows.Forms.Label();
            this.Y = new System.Windows.Forms.Label();
            this.Att = new System.Windows.Forms.Label();
            this.XInput = new System.Windows.Forms.TextBox();
            this.AttInput = new System.Windows.Forms.TextBox();
            this.YInput = new System.Windows.Forms.TextBox();
            this.CreVertex = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // X
            // 
            this.X.AutoSize = true;
            this.X.Location = new System.Drawing.Point(45, 29);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(26, 28);
            this.X.TabIndex = 0;
            this.X.Text = "X";
            // 
            // Y
            // 
            this.Y.AutoSize = true;
            this.Y.Location = new System.Drawing.Point(384, 29);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(25, 28);
            this.Y.TabIndex = 1;
            this.Y.Text = "Y";
            // 
            // Att
            // 
            this.Att.AutoSize = true;
            this.Att.Location = new System.Drawing.Point(672, 29);
            this.Att.Name = "Att";
            this.Att.Size = new System.Drawing.Size(54, 28);
            this.Att.TabIndex = 2;
            this.Att.Text = "属性";
            // 
            // XInput
            // 
            this.XInput.Location = new System.Drawing.Point(91, 26);
            this.XInput.Name = "XInput";
            this.XInput.Size = new System.Drawing.Size(175, 34);
            this.XInput.TabIndex = 3;
            // 
            // AttInput
            // 
            this.AttInput.Location = new System.Drawing.Point(747, 26);
            this.AttInput.Name = "AttInput";
            this.AttInput.Size = new System.Drawing.Size(175, 34);
            this.AttInput.TabIndex = 4;
            // 
            // YInput
            // 
            this.YInput.Location = new System.Drawing.Point(424, 26);
            this.YInput.Name = "YInput";
            this.YInput.Size = new System.Drawing.Size(175, 34);
            this.YInput.TabIndex = 5;
            // 
            // CreVertex
            // 
            this.CreVertex.Location = new System.Drawing.Point(986, 23);
            this.CreVertex.Name = "CreVertex";
            this.CreVertex.Size = new System.Drawing.Size(131, 40);
            this.CreVertex.TabIndex = 6;
            this.CreVertex.Text = "添加点实体";
            this.CreVertex.UseVisualStyleBackColor = true;
            this.CreVertex.Click += new System.EventHandler(this.CreVertex_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 28F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 711);
            this.Controls.Add(this.CreVertex);
            this.Controls.Add(this.YInput);
            this.Controls.Add(this.AttInput);
            this.Controls.Add(this.XInput);
            this.Controls.Add(this.Att);
            this.Controls.Add(this.Y);
            this.Controls.Add(this.X);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label X;
        private System.Windows.Forms.Label Y;
        private System.Windows.Forms.Label Att;
        private System.Windows.Forms.TextBox XInput;
        private System.Windows.Forms.TextBox AttInput;
        private System.Windows.Forms.TextBox YInput;
        private System.Windows.Forms.Button CreVertex;
    }
}

