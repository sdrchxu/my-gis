﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyGIS;

namespace test_1_2
{
    public partial class Form1 : Form
    {
        List<GISPoint> points = new List<GISPoint>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void CreVertex_Click(object sender, EventArgs e)
        {
            double x = Convert.ToDouble(XInput.Text);
            double y = Convert.ToDouble(YInput.Text);
            string attribute = AttInput.Text;
            GISVertex onevertex = new GISVertex(x, y);
            GISPoint onepoint = new GISPoint(onevertex, attribute);
            Graphics graphics = this.CreateGraphics();
            onepoint.DrawPoint(graphics);
            onepoint.DrawAttribute(graphics);
            points.Add(onepoint);
        }
    }
}
