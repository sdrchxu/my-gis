﻿using MyGIS;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;



namespace test_2_2
{
    public partial class Form1 : Form
    {
        List<GISFeature> features = new List<GISFeature>();
        public Form1()
        {
            InitializeComponent();
        }

        private void CreateVertex_Click(object sender, EventArgs e)
        {
            double x = Convert.ToDouble(XInput.Text);
            double y = Convert.ToDouble(YInput.Text);//X,Y坐标输入
            GISVertex onevertex = new GISVertex(x, y);
            GISPoint onepoint = new GISPoint(onevertex);//创建点实体
            string attribute = AttInput.Text;
            GISAttribute oneattribute = new GISAttribute();//创建属性值
            oneattribute.AddValue(attribute);
            GISFeature onefeature = new GISFeature(onepoint, oneattribute);//新建GISFeature,并添加到数组feature中
            features.Add(onefeature);
            Graphics graphics = this.CreateGraphics();
            onefeature.draw(graphics, true, 0);//在屏幕上画出空间实体
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            GISVertex onevertex = new GISVertex((double)e.X, (double)e.Y);
            double mindistance = double.MaxValue;
            int findid = -1;
            for (int i = 0; i < features.Count; i++)
            {
                double distance = features[i].spatialpart.centroid.Distance(onevertex);
                if (distance < mindistance)
                {
                    mindistance = distance;
                    findid = i;
                }
            }
            if (mindistance > 5 || findid == -1)
            {
                MessageBox.Show("没有点实体或鼠标点击位置不准确");
            }
            else
                MessageBox.Show(features[findid].getAttribute(0).ToString());

        }
    }
}
