﻿using MyGIS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace MyGIS
{
    public class GISVertex   //节点类
    {
        public double x;
        public double y;
        public GISVertex(double _x, double _y)
        {
            x = _x;
            y = _y;
        }//外部传参，向x与y赋值
        public double Distance(GISVertex anothervertex)
        {
            return Math.Sqrt((x - anothervertex.x) *
                (x - anothervertex.x) + (y - anothervertex.y) * (y - anothervertex.y));
        }//计算两点间距离
    }
    public class GISPoint : GISSpatial  //点实体
    {

        public GISPoint(GISVertex onevertex)
        {
            centroid = onevertex;
            extent = new GISExtent(onevertex, onevertex);
        }//实例化点实体
        public override void draw(Graphics graphics)
        {
            graphics.FillEllipse(new SolidBrush(Color.Red),
                new Rectangle((int)(centroid.x) - 3, (int)(centroid.y) - 3, 6, 6));
        }//在屏幕上画出点实体
        public double Distance(GISVertex anothervertex)
        {
            return centroid.Distance(anothervertex);
        }//计算两点间距离
    }
    public class GISLine : GISSpatial
    {
        List<GISVertex> AllVertexs;
        public override void draw(Graphics graphics)
        {

        }
    }
    public class GISPolygon : GISSpatial
    {
        List<GISVertex> AllVertexs;
        public override void draw(Graphics graphics)
        {

        }
    }


    public class GISFeature   //空间要素特征类
    {
        public GISSpatial spatialpart;  //对象的空间信息
        public GISAttribute attributepart;  //对象的属性信息
        public GISFeature(GISSpatial spatial, GISAttribute attribute)
        {
            spatialpart = spatial;
            attributepart = attribute;
        }//用于赋值的构造函数
        public void draw(Graphics graphics, bool DrawAttributeOrNot, int index)//index是需要画的属性信息的序列位置
        {
            spatialpart.draw(graphics);
            if (DrawAttributeOrNot)
            {
                attributepart.Draw(graphics, spatialpart.centroid, index);
            }
        }
        public object getAttribute(int index)
        {
            return attributepart.GetValue(index);
        }//获取属性
    }


    public class GISAttribute//空间对象属性类
    {
        public ArrayList values = new ArrayList();//创建一个数组用于存储属性信息
        public void AddValue(object o)  //添加属性值
        {
            values.Add(o);
        }
        public object GetValue(int index)  //获取属性值
        {
            return values[index];
        }
        public void Draw(Graphics graphics, GISVertex location, int index)
        {
            graphics.DrawString(values[index].ToString(), new Font("宋体", 20),
                new SolidBrush(Color.Green), new PointF((int)(location.x), (int)(location.y)));
        }//画出属性信息
    }


    public abstract class GISSpatial  //抽象空间要素类
    {
        public GISVertex centroid;//空间实体的中心点
        public GISExtent extent;//空间范围：最小外接矩形
        public abstract void draw(Graphics graphics);
    }


    public class GISExtent   //空间对象面积类
    {
        public GISVertex bottomleft;
        public GISVertex upright;
        public GISExtent(GISVertex _bottomleft, GISVertex _upright)
        {
            bottomleft = _bottomleft;
            upright = _upright;
        }
        public double getMinX()
        {
            return bottomleft.x;
        }
        public double getMaxX()
        {
            return upright.x;
        }
        public double getMinY()
        {
            return bottomleft.y;
        }
        public double getMaxY()
        {
            return upright.y;
        }
        public double getWidth()
        {
            return upright.x - bottomleft.x;
        }
        public double getHeight()
        {
            return upright.y - bottomleft.y;
        }
    }



    public class GISView   //空间对象浏览类
    {
        GISExtent CurrentMapExtent;  //记录显示的地图范围
        Rectangle MapWindowSize;  //记录绘图窗口的大小 
        double MapMinX, MapMinY;
        int WinW, WinH;
        double MapW, MapH;
        double ScaleX, ScaleY;
        public GISView(GISExtent _extent,Rectangle _rectangle)
        {
            Update(_extent, _rectangle);
        }
        public void Update(GISExtent _extent, Rectangle _rectangle)
        {
            CurrentMapExtent = _extent;
            MapWindowSize = _rectangle;
            MapMinX = CurrentMapExtent.getMinX();
            MapMinY = CurrentMapExtent.getMinY();
            WinW = MapWindowSize.Width;
            WinH = MapWindowSize.Height;
            MapW = CurrentMapExtent.getWidth();
            MapH = CurrentMapExtent.getHeight();
            ScaleX = MapW / WinW;
            ScaleY = MapH / WinH;
        }

        public Point ToScreenPoint(GISVertex onevertex)
        {
            double ScreenX = (onevertex.x - MapMinX) / ScaleX;
            double ScreenY = (onevertex.y - MapMinY) / ScaleY;
            return new Point((int)ScreenX, (int)ScreenY);
        }

        public GISVertex ToMapVertex(Point point)
        {
            double MapX = ScaleX = ScaleX * point.X + MapMinX;
            double MapY = ScaleY = ScaleY * point.Y + MapMinY;
            return new GISVertex(MapX, MapY);
        }
    }

}
