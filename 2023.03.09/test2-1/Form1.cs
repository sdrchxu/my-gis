﻿using System;
using System.Windows.Forms;

namespace test2_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == "" || textBox2.Text == "")
            {
                 MessageBox.Show("请输入字符串内容与密钥");
                return;
            }//检验是否输入了空值
            char key = textBox2.Text[0];//对密钥赋值
            string s= "";
            foreach (char c in textBox1.Text)
                s += (char)(c ^ key);
            textBox1.Text = s;
        }
    }
}
