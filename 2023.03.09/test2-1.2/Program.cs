﻿using System;

namespace test2_1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] iArray = { 20, -1, 0, 1, 2, 30, -8, 5, -10, 20, 45, 40, 50 };
            Console.WriteLine("初始数组：");
            CzArray.output(iArray);
            Console.Write("请输入要查找的整数：");
            int x;
            if (!int.TryParse(Console.ReadLine(), out x))
                return;
            int pos = CzArray.Search(iArray, x);//调用search方法，找出x在数组中的位置
            if (pos >= 0)
                Console.WriteLine("{0}在数组中的位置为{1}", x, pos);
            else
                Console.WriteLine("{0}不存在于数组中", x);
            Console.WriteLine("执行数组排序，排序后的数组：");
            CzArray.冒泡排序(iArray);//C#也可支持其它语言作为标识符
            CzArray.output(iArray);
            Console.WriteLine("请输入要查找的整数：");
            x = int.Parse(Console.ReadLine());//排序后进行二分法查找
            int result = CzArray.BinSearch(iArray, 0, iArray.Length - 1, x);
            if (result == -1)
                Console.WriteLine("查无此数");
            else
                Console.WriteLine(x + "在数组中的位置为" + result);
            Console.ReadLine();
        }
    }
    public class CzArray
    {
        public static void output(int[] iArray)
        {
            for (int i = 0; i < iArray.Length; i++)
                Console.Write(iArray[i] + " ");
            Console.WriteLine(" ");
        }//输出数组

        public static int Search(int[] iArray, int target)
        {
            for (int i = 0; i < iArray.Length; i++)
            {
                if (target == iArray[i])
                    return i;
            }
            return -1;
        }//逐个查找
        public static void InsertSort(int[] iArray)
        {
            int temp;
            for (int i = 1; i < iArray.Length; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if (iArray[i] < iArray[j])
                    {
                        temp = iArray[i];
                        for (int k = i; k > j; k--)
                        {
                            iArray[k] = iArray[k - 1];
                        }
                        iArray[j] = temp;
                    }
                }
            }
        }//插入排序

        public static void 冒泡排序(int[] iArray)
        {
            int temp;
            for (int i = iArray.Length - 1; i >= 0; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    if (iArray[j] > iArray[i])
                    {
                        temp = iArray[i];
                        iArray[i] = iArray[j];
                        iArray[j] = temp;
                    }
                }
            }
        }//冒泡排序

        public static int BinSearch(int[] iArray, int left, int right, int x)
        {
            if (right >= left)
            {
                int mid = left + (right - left) / 2;
                if (iArray[mid] == x)
                    return mid;
                if (iArray[mid] > x)
                    return BinSearch(iArray, left, mid - 1, x);
                if (iArray[mid] < x)
                    return BinSearch(iArray, mid + 1, right, x);
            }
            return -1;
        }
    }//二分法查找
}
