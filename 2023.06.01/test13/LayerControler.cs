﻿using MyGIS;
using System;
using System.Windows.Forms;

namespace test13
{
    public partial class LayerControler : Form
    {
        GISDocument Document;
        Form1 MapWindow;
        public LayerControler(GISDocument document, Form1 mapWindow)
        {
            InitializeComponent();
            Document = document;
            MapWindow = mapWindow;
        }


        private void LayerControler_Shown(object sender, EventArgs e)
        {
            for (int i = 0; i < Document.layers.Count; i++)
            {
                LayerListBox.Items.Insert(0, Document.layers[i].Name);
            }
            //令layerListBox的缺省选择项为第一项
            if (Document.layers.Count > 0)
                LayerListBox.SelectedIndex = 0;
        }


        /// <summary>
        /// LayerListBox选择项被更改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayerListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (LayerListBox.SelectedItem == null)
                return;
            GISLayer layer = Document.GetLayer(LayerListBox.SelectedItem.ToString());
            SelectableCheckBox.Checked = layer.Selectable;
            VisibleCheckBox.Checked = layer.Visible;
            DrawAttCheckBox.Checked = layer.DrawAttributeOrNot;
            for (int i = 0; i < layer.Fields.Count; i++)
            {
                FieldComboBox.Items.Add(layer.Fields[i].name);
            }
            FieldComboBox.SelectedIndex = layer.LabelIndex;
            fileAddresslabel.Text = layer.Path;
            LayerNameTextBox.Text = layer.Name;
            insideColorButton.BackColor = layer.Thematic.InsideColor;
            sizeTextBox.Text = layer.Thematic.Size.ToString();
            outsideColorButton.BackColor = layer.Thematic.OutSizeColor; 
        }


        private void Clicked(object sender, EventArgs e)
        {
            if (LayerListBox.SelectedItem == null)
                return;
            GISLayer layer = Document.GetLayer(LayerListBox.SelectedItem.ToString());
            layer.Selectable = SelectableCheckBox.Checked;
            layer.Visible = VisibleCheckBox.Checked;
            layer.DrawAttributeOrNot = DrawAttCheckBox.Checked;
            layer.LabelIndex = FieldComboBox.SelectedIndex;
            layer.Thematic.InsideColor = insideColorButton.BackColor;
            layer.Thematic.OutSizeColor = outsideColorButton.BackColor;
            if (Int32.Parse(sizeTextBox.Text) > 0)
                layer.Thematic.Size = Int32.Parse(sizeTextBox.Text);
        }


        /// <summary>
        /// “修改”按钮被按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModifyButton_Click(object sender, EventArgs e)
        {
            if (LayerListBox.SelectedItem == null)
                return;
            //检查输入名称是否唯一
            for (int i = 0; i < LayerListBox.Items.Count; i++)
            {
                if (i != LayerListBox.SelectedIndex)
                {
                    if (LayerListBox.Items[i].ToString() == LayerNameTextBox.Text)
                    {
                        MessageBox.Show("不能与已有图层名重复！");
                        return;
                    }
                }
            }
            GISLayer layer = Document.GetLayer(LayerListBox.SelectedItem.ToString());
            layer.Name = LayerNameTextBox.Text;
            LayerListBox.SelectedItem = LayerNameTextBox.Text;
        }


        /// <summary>
        /// “添加图层”按钮被按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddLayerButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "GIS Files(*." + GISConst.SHPFILE + ",*." + GISConst.MYFILE + ")|*." + GISConst.SHPFILE + ";* ." + GISConst.MYFILE;
            openFileDialog.RestoreDirectory = false;
            openFileDialog.FilterIndex = 1;
            openFileDialog.Multiselect = false;
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;
            GISLayer layer = Document.AddLayer(openFileDialog.FileName);
            LayerListBox.Items.Insert(0, layer.Name);
            LayerListBox.SelectedIndex = 0;
        }



        /// <summary>
        /// “上移”按钮被按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveUpButton_Click(object sender, EventArgs e)
        {
            //无选择
            if (LayerListBox.SelectedItem == null)
                return;
            //当前选择无法上移
            if (LayerListBox.SelectedIndex == 0)
                return;
            //当前图层名
            string selectedName = LayerListBox.SelectedItem.ToString();
            //需要调换的图层名
            string upperName = LayerListBox.Items[LayerListBox.SelectedIndex - 1].ToString();
            //在LayerListBox中完成调换
            LayerListBox.Items[LayerListBox.SelectedIndex - 1] = selectedName;
            LayerListBox.Items[LayerListBox.SelectedIndex] = upperName;
            //在Document中完成调换
            Document.SwitchLayer(selectedName, upperName);
            LayerListBox.SelectedIndex--;

        }




        /// <summary>
        /// “下移”按钮被按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveDownButton_Click(object sender, EventArgs e)
        {
            if (LayerListBox.SelectedItem == null)
                return;
            if (LayerListBox.Items.Count == 1)
                return;
            if (LayerListBox.SelectedIndex == LayerListBox.Items.Count - 1)
                return;
            string selectedName = LayerListBox.SelectedItem.ToString();
            string lowerName = LayerListBox.Items[LayerListBox.SelectedIndex + 1].ToString();
            LayerListBox.Items[LayerListBox.SelectedIndex + 1] = selectedName;
            LayerListBox.Items[LayerListBox.SelectedIndex] = lowerName;
            Document.SwitchLayer(selectedName, lowerName);
            LayerListBox.SelectedIndex++;
        }


        /// <summary>
        /// “导出图层”按钮被按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportLayerButton_Click(object sender, EventArgs e)
        {
            if (LayerListBox.SelectedItem == null)
                return;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "GIS file(*." + GISConst.MYFILE + ")|*." + GISConst.MYFILE;
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = false;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                GISLayer layer = Document.GetLayer(LayerListBox.SelectedItem.ToString());
                GISMyFile.WriteFile(layer, saveFileDialog1.FileName);
                MessageBox.Show("导出图层完成！");

            }
        }



        /// <summary>
        /// “存储文档”按钮被按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveFileButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "GIS Document" + "|*." + GISConst.MYDOC;
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = false;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Document.Write(saveFileDialog1.FileName);
                MessageBox.Show("存储文档完成！");
            }
        }

        private void OpenTableButton_Click(object sender, EventArgs e)
        {
            if (LayerListBox.SelectedItem == null)
                return;
            GISLayer layer = Document.GetLayer(LayerListBox.SelectedItem.ToString());
            MapWindow.OpenAttributeWindow(layer);
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            MapWindow.UpdateMap();
        }



        private void CloseButton_Click(object sender, EventArgs e)
        {
            MapWindow.UpdateMap();
            Close();
        }


        private void DeleteLayerButton_Click(object sender, EventArgs e)
        {
            if (LayerListBox.SelectedItem == null)
                return;
            Document.RemoveLayer(LayerListBox.SelectedItem.ToString());
            LayerListBox.Items.Remove(LayerListBox.SelectedItem);
            if (LayerListBox.Items.Count > 0)
                LayerListBox.SelectedIndex = 0;
        }



        private void SettingColor_Click(object sender,EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            colorDialog.Color = ((Button)sender).BackColor;//令初始颜色为按钮背景颜色
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                ((Button)sender).BackColor = colorDialog.Color;//用新的颜色更新
                Clicked(sender, e);
            }
        }

    }


}
