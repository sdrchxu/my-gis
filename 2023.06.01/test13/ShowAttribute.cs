﻿using MyGIS;
using System;
using System.Windows.Forms;

namespace test13
{
    public partial class ShowAttribute : Form
    {
        GISLayer Layer;
        Form1 MapWindow = null;
        bool FromMapWindow = true;//选择集变化的来源，true表示来自地图窗口
        public ShowAttribute(GISLayer layer, Form1 mapwindow)
        {
            InitializeComponent();
            Layer = layer;
            MapWindow = mapwindow;
        }

        private void ShowAttribute_Shown(object sender, EventArgs e)
        {
            FromMapWindow = true;
            FillValue();
            FromMapWindow = false;
        }



        /// <summary>
        /// 为属性表添加值
        /// </summary>
        private void FillValue()
        {
            dataGridView1.Columns.Add("ID", "ID");//添加ID列
            for (int i = 0; i < Layer.Fields.Count; i++)//添加其它列
            {
                dataGridView1.Columns.Add(Layer.Fields[i].name, Layer.Fields[i].name);//（字段名，显示在表头上的文字）
            }
            for (int i = 0; i < Layer.FeatureCount(); i++)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[i].Cells[0].Value = Layer.GetFeature(i).ID;//添加ID值
                for (int j = 0; j < Layer.Fields.Count; j++)
                {
                    dataGridView1.Rows[i].Cells[j + 1].Value = Layer.GetFeature(i).GetAttribute(j);//添加其它属性值
                }
                dataGridView1.Rows[i].Selected = Layer.GetFeature(i).Selected;//设定每行的选定状态
            }
        }



        /// <summary>
        /// 更新属性表中的数据
        /// </summary>
        public void UpdateData()
        {
            FromMapWindow = true;
            dataGridView1.ClearSelection();
            foreach (GISFeature feature in Layer.Selection)
            {
                SelectRowByID(feature.ID).Selected = true;
            }
            FromMapWindow = false;
        }
        public DataGridViewRow SelectRowByID(int ID)//通过ID找到行
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((int)(row.Cells[0].Value) == ID)
                    return row;
            }
            return null;
        }



        /// <summary>
        /// 选择集出现变化的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (FromMapWindow == true)//如果选择集的变化来自地图窗口
                return;
            if (Layer.Selection.Count == 0 && dataGridView1.SelectedRows.Count == 0)//如果两个窗口的当前选择集均为空
                return;
            //更新地图窗口的选择集
            Layer.ClearSelection();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                if (row.Cells[0].Value != null)//表最后一行的空值检验
                    Layer.AddSelectedFeatureByID((int)(row.Cells[0].Value));
            }
            MapWindow.UpdateMap();//更新地图窗口的显示
        }
    }
}
