﻿using MyGIS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;

namespace MyGIS
{


    public class GISVertex   //节点类

    {
        public double x;
        public double y;
        public GISVertex(double _x, double _y)
        {
            x = _x;
            y = _y;
        }//外部传参，向x与y赋值
        public double Distance(GISVertex anothervertex)
        {
            return Math.Sqrt((x - anothervertex.x) *
                (x - anothervertex.x) + (y - anothervertex.y) * (y - anothervertex.y));
        }//计算两点间距离


        public void CopyFrom(GISVertex v)//使用输入点替换当前点
        {
            x = v.x;
            y = v.y;
        }
    }


    public class GISPoint : GISSpatial  //点实体
    {

        public GISPoint(GISVertex onevertex)
        {
            centroid = onevertex;
            extent = new GISExtent(onevertex, onevertex);
        }//实例化点实体
        public override void draw(Graphics graphics,GISView view)
        {
            Point screenpoint = view.ToScreenPoint(centroid);
            graphics.FillEllipse(new SolidBrush(Color.Red),
                new Rectangle(screenpoint.X - 3, screenpoint.Y - 3, 6, 6));
        }//在屏幕上画出点实体
        public double Distance(GISVertex anothervertex)
        {
            return centroid.Distance(anothervertex);
        }//计算两点间距离
    }

    /// <summary>
    /// 线实体
    /// </summary>
    public class GISLine : GISSpatial
    {
        List<GISVertex> Vertexs;
        public double Length;
        public GISLine(List<GISVertex> _vertexs)
        {
            if (_vertexs == null || _vertexs.Count < 2)
            {
                throw new ArgumentException("无效的输入节点");
            }
            Vertexs = _vertexs;
            centroid = GISTools.CalculateCentroid(_vertexs);
            extent = GISTools.CalculateExtent(_vertexs);
            Length = GISTools.CalculateLength(_vertexs);
        }
        public override void draw(Graphics graphics,GISView view)//画出Line
        {
            Point[] points = GISTools.GetScreenPoint(Vertexs, view);
            graphics.DrawLines(new Pen(Color.Red, 2), points);

        }
        public GISVertex FromNode()//返回起始结点
        {
            return Vertexs[0];
        }
        public GISVertex ToNode()//返回终止结点
        {
            return Vertexs[Vertexs.Count - 1];
        }
    }

    /// <summary>
    /// 面实体
    /// </summary>
    public class GISPolygon : GISSpatial
    {
        List<GISVertex> Vertexs;
        public double Area;
        public GISPolygon(List<GISVertex> _vertexs)
        {
            if (_vertexs == null || _vertexs.Count < 3)//当输入节点数不符合要求时抛出异常
            {
                throw new ArgumentException("无效的输入节点");
            }
            Vertexs = _vertexs;
            centroid = GISTools.CalculateCentroid(_vertexs);
            Area = GISTools.CalculateArea(_vertexs);
            extent = GISTools.CalculateExtent(_vertexs);
        }
        public override void draw(Graphics graphics,GISView view)//画出Polygon
        {
            Point[] points = GISTools.GetScreenPoint(Vertexs,view);
            graphics.FillPolygon(new SolidBrush(Color.LightSteelBlue), points);
            graphics.DrawPolygon(new Pen(Color.White, 2), points);
        }
    }


    public class GISFeature   //空间要素特征类
    {
        public GISSpatial spatialpart;  //对象的空间信息
        public GISAttribute attributepart;  //对象的属性信息
        public GISFeature(GISSpatial spatial, GISAttribute attribute)
        {
            spatialpart = spatial;
            attributepart = attribute;
        }
        public void draw(Graphics graphics,GISView view, bool DrawAttributeOrNot, int index)
        {
            spatialpart.draw(graphics,view);
            if (DrawAttributeOrNot)
            {
                attributepart.draw(graphics,view, spatialpart.centroid, index);
            }
        }
        public object getAttribute(int index)
        {
            return attributepart.GetValue(index);
        }
    }


    public class GISAttribute
    {
        public ArrayList values = new ArrayList();
        public void AddValue(object o)  //添加属性值
        {
            values.Add(o);
        }
        public object GetValue(int index)  //获取属性值
        {
            return values[index];
        }
        public void draw(Graphics graphics, GISView view, GISVertex location,int index)
        {
            Point screenpoint = view.ToScreenPoint(location);
            graphics.DrawString(values[index].ToString(),
                new Font("宋体", 20),
                new SolidBrush(Color.Green), 
                new PointF(screenpoint.X,screenpoint.Y));
        }//画出属性信息
    }


    public abstract class GISSpatial  //抽象空间要素类
    {
        public GISVertex centroid;//空间实体的中心点
        public GISExtent extent;//空间范围：最小外接矩形
        public abstract void draw(Graphics graphics,GISView view);
    }


    public class GISExtent   //空间对象面积类
    {
        public GISVertex bottomleft;
        public GISVertex upright;
        public double ZoomingFactor=2 ;//缩放系数，放大或缩小时会缩放为2倍或二分之一
        public double MovingFactor=0.25 ;//移动系数，即移动一次会有四分之一的屏幕范围移除屏幕

        /// <summary>
        /// 显示范围（左下角点，右上角点）
        /// </summary>
        /// <param name="_bottomleft"></param>
        /// <param name="_upright"></param>
        public GISExtent(GISVertex _bottomleft, GISVertex _upright)
        {
            bottomleft = _bottomleft;
            upright = _upright;
        }
        public double getMinX()
        {
            return bottomleft.x;
        }
        public double getMaxX()
        {
            return upright.x;
        }
        public double getMinY()
        {
            return bottomleft.y;
        }
        public double getMaxY()
        {
            return upright.y;
        }
        public double getWidth()  //计算地图横坐标长度
        {
            return upright.x - bottomleft.x;
        }
        public double getHeight()  //计算地图纵坐标长度
        {
            return upright.y - bottomleft.y;
        }
        public void ChangeExtent(GISMapActions actions)
        {
            double newminx = bottomleft.x, newminy = bottomleft.y,
                newmaxx = upright.x, newmaxy = upright.y;
            switch (actions)
            {
                case GISMapActions.zoomin:  //地图放大
                    newminx = ((getMinX() + getMaxX()) - getWidth() / ZoomingFactor) / 2;
                    newminy = ((getMinY() + getMaxY()) - getHeight() / ZoomingFactor) / 2;
                    newmaxx = ((getMinX() + getMaxX()) + getWidth() / ZoomingFactor) / 2;
                    newmaxy = ((getMinY() + getMaxY()) + getHeight() / ZoomingFactor) / 2;
                    break;
                case GISMapActions.zoomout:  //地图缩小
                    newminx = ((getMinX() + getMaxX()) - getWidth() * ZoomingFactor) / 2;
                    newminy = ((getMinY() + getMaxY()) - getHeight() * ZoomingFactor) / 2;
                    newmaxx = ((getMinX() + getMaxX()) + getWidth() *ZoomingFactor) / 2;
                    newmaxy = ((getMinY() + getMaxY()) + getHeight()* ZoomingFactor) / 2;
                    break;
                case GISMapActions.moveup:  //向上移动，即地图范围下移
                    newminy = getMinY() - getHeight() * MovingFactor;
                    newmaxy = getMaxY() - getHeight() * MovingFactor;
                    break;
                case GISMapActions.movedown:  //向下移动，即地图范围上移
                    newminy = getMinY() + getHeight() * MovingFactor;
                    newmaxy = getMaxY() + getHeight() * MovingFactor;
                    break;
                case GISMapActions.moveleft:  //向左移动
                    newminx = getMinX() + getWidth() * MovingFactor;
                    newmaxx = getMaxX() + getWidth() * MovingFactor;
                    break;
                case GISMapActions.moveright:  //向右移动
                    newminx = getMinX() - getWidth() * MovingFactor;
                    newmaxx = getMaxX() - getWidth() * MovingFactor;
                    break;
            }
            upright.x = newmaxx;
            upright.y = newmaxy;
            bottomleft.x = newminx;
            bottomleft.y = newminy;
        }

        public void CopyFrom(GISExtent extent)//使用Extent替换当前值
        {
            upright.CopyFrom(extent.upright);
            bottomleft.CopyFrom(extent.bottomleft);
        }
    }



    public class GISView   //空间对象浏览类
    {
       GISExtent CurrentMapExtent;//记录显示的地图范围
        Rectangle MapWindowSize;  //记录绘图窗口的大小 
        double MapMinX, MapMinY;
        int WinW, WinH;
        double MapW, MapH;
        double ScaleX, ScaleY;
        public GISView(GISExtent _extent, Rectangle _rectangle)
        {
            UpdateMap(_extent, _rectangle);
        }

        /// <summary>
        /// 更新地图
        /// </summary>
        /// <param name="_extent"></param>
        /// <param name="_rectangle"></param>
        public void UpdateMap(GISExtent _extent, Rectangle _rectangle)
        {
            CurrentMapExtent = _extent;//当前地图范围
            MapWindowSize = _rectangle;
            MapMinX = CurrentMapExtent.getMinX();//地图最小X值
            MapMinY = CurrentMapExtent.getMinY();
            WinW = MapWindowSize.Width;//窗口宽度
            WinH = MapWindowSize.Height;//窗口高度
            MapW = CurrentMapExtent.getWidth();
            MapH = CurrentMapExtent.getHeight();
            ScaleX = MapW / WinW;//比例尺=图上距离/实际距离
            ScaleY = MapH / WinH;
        }//更新地图

        public void UpdateExtent(GISExtent extent)//更新当前地图范围
        {
            CurrentMapExtent.CopyFrom(extent);
            UpdateMap(CurrentMapExtent, MapWindowSize);
        }

        public Point ToScreenPoint(GISVertex onevertex)//地图转屏幕点
        {
            double ScreenX = (onevertex.x - MapMinX) / ScaleX;
            double ScreenY = WinH-(onevertex.y - MapMinY) / ScaleY;
            return new Point((int)ScreenX, (int)ScreenY);
        }

        public GISVertex ToMapVertex(Point point)//屏幕转地图点
        {
            double MapX = ScaleX*point.X + MapMinX;
            double MapY =ScaleY * (WinH-point.Y) + MapMinY;
            return new GISVertex(MapX, MapY);
        }

        public void ChangeView(GISMapActions actions)//改变显示范围
        {
            CurrentMapExtent.ChangeExtent(actions);
            UpdateMap(CurrentMapExtent, MapWindowSize);
        }
    }

    public enum GISMapActions  //地图的缩放与平移操作
    {
         zoomin,zoomout,
         moveup,movedown,moveleft,moveright
    }

    class GISTools
    {
        public static GISVertex CalculateCentroid(List<GISVertex> _vertex)//计算空间实体中心点
        {
            if (_vertex.Count == 0) return null;
            double x = 0;
            double y = 0;
            for (int i = 0; i < _vertex.Count; i++)
            {
                x += _vertex[i].x;
                y += _vertex[i].y;
            }
            return new GISVertex(x / _vertex.Count, y / _vertex.Count);
        }
        public static GISExtent CalculateExtent(List<GISVertex> _vertex)//计算空间实体范围
        {
            if (_vertex.Count == 0) return null;
            double minx = Double.MaxValue;
            double miny = Double.MaxValue;
            double maxx = Double.MinValue;
            double maxy = Double.MinValue;
            for (int i = 0; i < _vertex.Count; i++)
            {
                if (_vertex[i].x < minx) minx = _vertex[i].x;
                if (_vertex[i].y < miny) miny = _vertex[i].y;
                if (_vertex[i].x > maxx) maxx = _vertex[i].x;
                if (_vertex[i].y > maxy) maxy = _vertex[i].y;
            }
            return new GISExtent(new GISVertex(minx, miny), new GISVertex(maxx, maxy));
        }

        public static double CalculateLength(List<GISVertex> _vertex)//计算线长度或面的周长（折线长度）
        {
            double length = 0;
            for (int i = 0; i < _vertex.Count-1; i++)
            {
                length += _vertex[i].Distance(_vertex[i + 1]);
            }
            return length;
        }

        public static double CalculateArea(List<GISVertex> _vertex)//计算面实体的面积（利用矢量积计算）
        {
            double area = 0;
            for (int i = 0; i < _vertex.Count-1; i++)
            {
                area += VectorProduct(_vertex[i], _vertex[i + 1]);
            }
            return area / 2;
        }

        public static double VectorProduct(GISVertex v1,GISVertex v2)//矢量积的计算
        {
            return v1.x * v2.y - v1.y * v2.x;
        }


        /// <summary>
        /// 把节点按当前的GISView转换成屏幕坐标
        /// </summary>
        /// <param name="_vertexs"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        public static Point[] GetScreenPoint(List<GISVertex> _vertexs,GISView view)
        {
            Point[] points = new Point[_vertexs.Count];
            for(int i=0; i < points.Length; i++)
            {
                points[i] = view.ToScreenPoint(_vertexs[i]);//对每个点调用ToScreenPoint方法
            }
            return points;
        }
    }
}
