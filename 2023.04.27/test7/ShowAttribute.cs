﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyGIS;

namespace test7
{
    public partial class ShowAttribute : Form
    {
        public ShowAttribute(GISLayer layer)
        {
            InitializeComponent();
            for (int i = 0; i < layer.Fields.Count; i++)//添加列
            {
                dataGridView1.Columns.Add(layer.Fields[i].name, layer.Fields[i].name);//（字段名，显示在表头上的文字）
            }
            for (int i = 0; i < layer.FeatureCount(); i++)
            {
                dataGridView1.Rows.Add();//添加新行
                for (int j = 0; j < layer.Fields.Count; j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = layer.GetFeature(i).getAttribute(j);//添加行中的属性值
                }
            }
        }
    }
}
