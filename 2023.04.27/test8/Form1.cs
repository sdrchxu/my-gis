﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyGIS;
using test8;
using static MyGIS.GISMyFile;

namespace test8
{
    public partial class Form1 : Form
    {
        GISLayer Layer = null;
        GISView View = null;
        public Form1()
        {
            InitializeComponent();
            View = new GISView(new GISExtent(new GISVertex(0, 0), new GISVertex(100, 100)), ClientRectangle);
        }

        private void OpenFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Shapefile 文件|*.shp";//文件类型限制为Shapefile
            openFileDialog.RestoreDirectory = false;//不还原选择文件时更改的目录
            openFileDialog.FilterIndex = 1;
            openFileDialog.Multiselect = false;//不允许同时选中多个文件
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;
            Layer = GISShapefile.ReadShapefile(openFileDialog.FileName);
            Layer.DrawAttributeOrNot = false;
            MessageBox.Show("read" + Layer.FeatureCount() + "objects.");
            View.UpdateExtent(Layer.Extent);
            UpdateMap();
        }

        private void ShowMapButton_Click(object sender, EventArgs e)
        {
            View.UpdateExtent(Layer.Extent);
            UpdateMap();
        }

        private void UpdateMap()
        {
            Graphics graphics = CreateGraphics();
            graphics.FillRectangle(new SolidBrush(Color.Black), ClientRectangle);
            Layer.draw(graphics,View);
        }

        private void MapButtonClick(object sender, EventArgs e)
        {
            GISMapActions actions = GISMapActions.zoomin;
            if ((Button)sender == ZoomInButton) actions = GISMapActions.zoomin;
            else if ((Button)sender == ZoomOutButton) actions = GISMapActions.zoomout;
            else if ((Button)sender == MoveDownButton) actions = GISMapActions.movedown;
            else if ((Button)sender == MoveUpButton) actions = GISMapActions.moveup;
            else if ((Button)sender == MoveLeftButton) actions = GISMapActions.moveleft;
            else if ((Button)sender == MoveRightButton) actions = GISMapActions.moveright;
            View.ChangeView(actions);
            UpdateMap();
        }

        /// <summary>
        /// 更改缩放系数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoomingFactorBox_ValueChanged(object sender, EventArgs e)
        {
            Layer.Extent.ZoomingFactor = (double)ZoomingFactorBox.Value;
        }

        private void MovingFactorBox_ValueChanged(object sender, EventArgs e)
        {
            Layer.Extent.MovingFactor = (double)MovingFactorBox.Value;
        }

        private void OpenTableButton_Click(object sender, EventArgs e)
        {
            ShowAttribute form = new ShowAttribute(Layer);
            form.Show();
        }

        private void SaveFileButton_Click(object sender, EventArgs e)
        {
            WriteFile(Layer, @"D:\VS2019\MyGISFile\mygisfile.rcx");
            MessageBox.Show("done");
        }

        private void ReadFileButton_Click(object sender, EventArgs e)
        {
            Layer = ReadFile(@"D:\VS2019\MyGISFile\mygisfile.rcx");
            MessageBox.Show("read" + Layer.FeatureCount() + "objects");
            View.UpdateExtent(Layer.Extent);
            UpdateMap();
        }
    }
}
